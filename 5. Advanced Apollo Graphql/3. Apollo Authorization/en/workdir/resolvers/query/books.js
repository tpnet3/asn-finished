const Book = require('./../../db/models/books.js');

const books = async (obj, {pageSize, next})=>{
  const result = await Book.books(pageSize, next);
  if(!result){
    return null
  }
  else{
    return result
  }
}

const getSingleBook = async (obj, {price})=>{
  const result = await Book.getSingleBook(price);
  if(result===null){
  return null;
  }
  return result;
}
module.exports = {
  books,//already exported before
  getSingleBook //newly created method
}