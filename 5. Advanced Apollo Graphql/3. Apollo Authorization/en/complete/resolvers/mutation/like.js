const Like = require('./../../db/models/liked');  //here we are assuming that the file where you have defined Liked Model to be "liked.js". If this is not the case make sure you change the name of directory in accordance with your naming convention

const likeBook = async (obj, {book_id}, ctx)=>{
  const result = await Like.likeBook(book_id, ctx);
  if(!result){
    throw new Error(`addToLikedNull`)
  }
  return result; 
}


module.exports = {
  likeBook
}