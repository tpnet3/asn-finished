const books = require('./query/books');
const liked = require('./mutation/like');
const userMutation = require('./mutation/user');
const userQuery = require('./query/user');
module.exports = {
    Query: {
        books: books.books,
        getSingleBook: books.getSingleBook,
        signInWithPassword: userQuery.signInWithPassword
    },
    Mutation: {
        signUpWithPassword: userMutation.signUpWithPassword,
        likeBook: liked.likeBook
    }
}