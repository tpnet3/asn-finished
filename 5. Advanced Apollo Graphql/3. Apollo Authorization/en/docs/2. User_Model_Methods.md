# User Model Static Methods

In this article we will add three static methods in User Model you have created yourself. These methods are: 
* signUpWithPassword 
* signInWithPassword
* verifyUserWithToken

>We assume that you have created user model inside user.js file in db/models directory.

### Creating signUpWithPassword() 
Before implementing the method let's discover how it is supposed to function: 
* It should get two arguments **username** and **password**
* It should check if **username** already exists in database. If **username** exists method should return relative error.
* It should insert new user with given **username** and **password**. When we insert new user documet to User model in mongoose, **Model.create()** returns newly created user document.
* It should make new **jwt token**  using _id of newly created user.
* It should return **jwt token** back to client.

One more thing before we start coding. **JWT** is npm package and needs to be installed in our project:
* Open Terminal and go to project directory then install jwt: 
    ```cmd
    npm install jsonwebtoken
    ```
* Create **secret key** for jwt in .env file in project's parent directory so that we can make token using it: 
  * Go to parent directory of the server project and create **.env** file. Yes exactly .env file just like **something.js**. Then insert following:
    ```js
    SECRET=8ehw9vh829hfv3892hv2389hv2389
    ```


 
Go to user.js in db/models directory bring jwt from jswonwebtoken and bring secret key from **.env** file : 
```js
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET || 'secret';
```
Now we can implement the method: 
```js
userSchema.statics.SignUpWithPassword = async function (username, password) {
  const userName = await this.findOne({username: username}).exec();
  
  if(userName){
    throw new Error(`usernameTaken`);
  }
  
  const user = await this.create({
    username: username,
    password: passwordCrypto(password),
  });
  
  if(!user){
    throw new Error("signUpFailed");
  }
  const expiresIn = 7776000;
  
  const accessToken = jwt.sign({
    _id: user._id,
  }, secret, {expiresIn: expiresIn}); //made token
  
  return accessToken;
}

```
The only thing which might not be very familiar should be **jwt.sign()** function. Therefore we recommend you to read its official documentation [here](https://github.com/auth0/node-jsonwebtoken#usage).<br>

### Creating signInWithPassword()

Before implementing the method let's discover how it is supposed to function: 
* It should get two arguments **username** and **password**
* It should find user with given username and check if the **password** in argument matches **password** in document found in db. If does not match method should return relative error to client.
* It should make new **jwt token**  using _id of user document found in db.
* It should return **jwt token** back to client.

Now we can implement the method: 
```js
userSchema.statics.signInWithPassword = async function (username, password) { 
  const user = await this.findOne({username: username}).exec();
  if(!user){
    throw new Error("userNotFound");
  }
  if(user.password === password){
    const expiresIn = 7776000;
    const accessToken = jwt.sign({
      _id: user._id,
    }, secret, {expiresIn: expiresIn}); //made token
    return accessToken;
  }
  else{
    throw new Error("passwordIncorrect");
  }
}
```

### Creating verifyUser()

Before implementing the method let's discover how it is supposed to function: 
* It should get **Json Web Token** as an argument.
* With given jwt as an argument it should decode token with **jwt.verify()** method. If you want to learn more about it we recommend you to read its official documentation [here](https://github.com/auth0/node-jsonwebtoken#jwtverifytoken-secretorpublickey-options-callback). If the jwt.verify() is successful it should return the same object we made when we created the token.
* It should find user with _id of user which is returned by **jwt.verify()**.
* It should return false if there is no user with a given _id.
* If there is a user document with given user _id, then found docuement should be returned. This means user is verified.

Now, let's implement this method: 
```js
userSchema.statics.verifyUser = async function (token) {   
  var decoded_object = await jwt.verify(token, secret);
  if(!decoded_object){
    return false;
  }
  var user = await this.findById({_id: decoded_object._id}).exec();
  if(!user){
    return false
  }
  return user;
}
```
It is worth to note that, the verifyUser function is not as a resolver function but it helps when you need to verify user in resolver. For example when a user wants to like a book you need verify user first. In that case you can take advantage of this method. 

In the article we will find out about adding context to Appollo Server.
