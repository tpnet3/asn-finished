# 사용자 모델 정적 메소드

이 장에서는 생성했던 사용자 모델에 세 가지 정적 메소드를 추가할 것 입니다. 여기서 다룰 메소드는 다음과 같습니다.
* signUpWithPassword 
* signInWithPassword
* verifyUserWithToken

>db/models 디렉토리 내에 user.js 파일의 사용자 모델을 생성했을 것 입니다.

### signUpWithPassword() 생성하기
메소드를 구현하기이전에 함수들이 어떻게 동작하는지 알아보겠습니다.


* **username** 와 **password** 두 개의 인자를 가집니다 
* 데이터베이스 내에 **username**이 이미 존재하는지 체크 합니다. 만약 **username** 이 존재한다면, 메소드는 관련된 에러를 반환할 것 입니다.
* 주어진 **username** 와 **password**와 함께 새로운 사용자를 삽입해야 합니다. 새로운 사용자 문사를 몽구스에 사용자 모델에 삽입할 때, **Model.create()** 는 새롭게 생성된 사용자 문서를 반환합니다. 
* 새롭게 생성된 사용자의 _id 를 사용하는 새로운 **jwt token**를 만들어야 합니다
* 클라이언트에게  **jwt token**를 반환합니다.


코딩에 앞서 하나 더 숙지해야 할 것이 있습니다. **JWT**은 npm 패키지이며, 프로젝트에 설치가 되어야 합니다.
* 터미널을 실행하여 프로젝트 디렉토리로 이동하여 jwt를 설치하세요
    ```cmd
    npm install jsonwebtoken
    ```
* 이걸 활용하여 토큰을 만들어야 하므로 프로젝트의 부모 디렉토리 내에 .env파일의 jew를 위한 **secret key**를 생성하세요
* 서버 프로젝트의 부모 디렉토리로 가서 **.env** 파일을 생성하세요. 물론 .env 파일은 단지 **something.js**와 같은 것 입니다. 그런 다음 다음을 삽입하세요 
    ```js
    SECRET=8ehw9vh829hfv3892hv2389hv2389
    ```


db/모델 디렉토리에 user.js로 가서 jswonwebtoken로부터 jwt를 가져와 **.env** file 로부터 비밀키를 가져오세요.

```js
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET || 'secret';
```
Now we can implement the method: 
```js
userSchema.statics.SignUpWithPassword = async function (username, password) {
  const userName = await this.findOne({username: username}).exec();
  
  if(userName){
    throw new Error(`usernameTaken`);
  }
  
  const user = await this.create({
    username: username,
    password: passwordCrypto(password),
  });
  
  if(!user){
    throw new Error("signUpFailed");
  }
  const expiresIn = 7776000;
  
  const accessToken = jwt.sign({
    _id: user._id,
  }, secret, {expiresIn: expiresIn}); //made token
  
  return accessToken;
}

```
친숙하지 않은건  **jwt.sign()**함수 일 것 입니다. 그러므로 공식 문서를 숙지하길 권장드립니다. 
[링크](https://github.com/auth0/node-jsonwebtoken#usage).<br>

### signInWithPassword() 생성하기

메소드를 실형하기 전에 어떻게 작동하는지 한번 살펴보도록 하겠습니다
* **username** 와 **password**의 두 인자를 받습니다
*  사용자이름이 주어진 사용자를 찾아 인자 내에 **password**가 매칭이 되는지 확인 해야 합니다. 
**db 내 문서에  **password**. 매칭이 되지 않는다면 메소드는 관련된 오류를 클라이언트에게 반환해야 합니다.  
* db에 사용자 문서의 _id를 이용한 새로운 **jwt token**를 생성합니다.
* 클라이언트에게 **jwt token**를 반환합니다.

이제 메소드를 구현할 수 있습니다
```js
userSchema.statics.signInWithPassword = async function (username, password) { 
  const user = await this.findOne({username: username}).exec();
  if(!user){
    throw new Error("userNotFound");
  }
  if(user.password === password){
    const expiresIn = 7776000;
    const accessToken = jwt.sign({
      _id: user._id,
    }, secret, {expiresIn: expiresIn}); //made token
    return accessToken;
  }
  else{
    throw new Error("passwordIncorrect");
  }
}
```

### verifyUser() 생성하기

메소를 구현하기 이전에, 어떻게 동작하는지 살펴보도록 하겠습니다
* 인자로  **Json Web Token** 을 얻습니다.
*인자로서 jew를 가지고  **jwt.verify()** 메소드의 토큰을 디코딩 해야 합니다. 더 학습해보기를 원한다면 이에 관련한 공식 문서를 읽어보기를 권장합니다
[링크](https://github.com/auth0/node-jsonwebtoken#jwtverifytoken-secretorpublickey-options-callback). jwt.verify()가 성공한다면, 토큰을 생성했을 때 만든 같은 객체를 반환해야 합니다.
* **jwt.verify()**에 반환된 사용자의 _id 를 가진 사용자를 찾습니다. 
* _id를 가진 어떠한 사용자도 없다면 false 를 반환합니다.
* 사용자 _id 가 주어진 사용자 문서가 있다면, 발견이 된 문서는 반환이 되어야 합니다. 사용자는 검증이 된다는 것을 의미합니다.

그럼 이제 메소드를 구현해 보도록 하죠 
```js
userSchema.statics.verifyUser = async function (token) {   
  var decoded_object = await jwt.verify(token, secret);
  if(!decoded_object){
    return false;
  }
  var user = await this.findById({_id: decoded_object._id}).exec();
  if(!user){
    return false
  }
  return user;
}
```


verifyUser 함수가 리졸버 함수와 같진 않지만 리졸버에서 사용자를 검증해야할 때 도움이 된다는 것을 잘 알고 있어야 합니다. 예를 들면 사용자가 책을 원할 때 사용자를 먼저 검증해야 합니다.  이 경우에 이 메소드의 장점을 잘 활용할 수 있는 것 입니다.

이 장에서는 아폴로서버에 컨택스트 추가를 하는 것에 대해 알아보도록 할 것 입니다.



