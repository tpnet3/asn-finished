const mongoose = require('mongoose');
// define a schema
const bookSchema = new mongoose.Schema({
  name: String,
  author: String,
  pics:[
    String
  ],
  price: Number,
  short_desc: String,
  long_desc: String,
  date_added: Number,
})

// assign a function to the "methods" object of our bookSchema
bookSchema.methods.findSamePriceBooks = function(price) {
  return this.model('Book').find({ price: this.price });
};

bookSchema.statics.findByName = function(name, cb) {
  return this.find({ name: new RegExp(name, 'i') }, cb);
};
bookSchema.statics.getBooks = async function () {
  const result = await this.find().exec(); //this will return array of books
  if(!result){
      return null
  }
  else{
      return result
  }
}
bookSchema.statics.getSingleBook = async function (price) {
  const result = await this.findOne({price: price}).exec(); //this will return a book with given argument
  if(!result){
      return null
  }
  else{
      return result
  }
}
bookSchema.statics.books = async function (pageSize, next) {
  var hasMore=true;
  var _next;
  var arr = [];
  var result = this.find({},{}, {skip: next-1, sort: { 'date_added' : -1 }}); //fetching the newest product first
  arr = await result;
  let len  = arr.length;
  if(len>=pageSize){
    result = arr.slice(0, pageSize);
    if(len===pageSize){
      hasMore=false
    }
  }
  else{
    result = arr;
    hasMore=false;
  }
  _next = next+pageSize;
  return {
    hasMore: hasMore,
    next: _next,
    books: result
  }
}
module.exports = mongoose.model('Book', bookSchema);