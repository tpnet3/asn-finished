const { ApolloServer, gql } = require('apollo-server-express');
const express = require('express')
const typeDefs = require('./schema');
const resolvers = require('./resolvers');
require('./db/index.js'); 

const context = ({ req }) => {
  const accessToken = req.headers.authorization || null; 
  return {
    req,
    headers: {
      accessToken: accessToken
    }
  }
};

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({ typeDefs, resolvers, context });

//in order to listen to requests 
const app = express();
server.applyMiddleware({ app });

// This `listen` method launches a web-server.  Existing apps
const port = process.env.PORT || 4001;

app.listen({ port }, () =>
  console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`),)