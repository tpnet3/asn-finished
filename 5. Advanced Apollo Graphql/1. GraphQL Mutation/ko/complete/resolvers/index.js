const books = require('./query/books')
const liked = require('./mutation/like')
module.exports = {
    Query: {
        books: books.books,
        getSingleBook: books.getSingleBook
    },
    Mutation: {
        likeBook: liked.likeBook
    }
}