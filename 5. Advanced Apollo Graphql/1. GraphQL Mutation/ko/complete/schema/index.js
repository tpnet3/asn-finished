const { gql } = require('apollo-server');
const types = require('./types');
const schema = gql`
  type Query {
      books: [Book],
      getSingleBook(
          price: Float
      ): Book
    },
  type Mutation {
      likeBook(
          book_id: String!,
          user_id: String!
      ): Boolean  
    }
  `
  module.exports = [schema, types];