# 구현
예전에도 다루었지만, 구현은 상향식입니다.

### 모델 내에 정적 페이지네이션 메소드 구현하기
* db/models 디렉토리 내에 boo.js file로 가서 다음의 *books* 정적 메소드를 추가하세요
```js
bookSchema.statics.books = async function (pageSize, next) {
  var hasMore=true;
  var _next;
  var arr = [];
  var result = this.find({},{}, {skip: next-1, sort: { 'date_added' : -1 }}); //fetching the newest product first
  arr = await result;
  let len  = arr.length;
  if(len>=pageSize){
    result = arr.slice(0, pageSize);
    if(len===pageSize){
      hasMore=false
    }
  }
  else{
    result = arr;
    hasMore=false;
  }
  _next = next+pageSize;
  return {
    hasMore: hasMore,
    next: _next,
    books: result
  }
}
```
한 줄 한 줄 코드를 자세히 보면 이해하기 쉬울 것 입니다. 그럼 코드를 각각 나뉘어서 살펴보도록 하겠습니다.
* 추후에 사용될 세 변수를 선언합니다. 
* 다음으로는 아래 조건들에 books 를 페칭하는 것입니다. 다음 라인을 이해하는 것이 중요합니다. 
    ```js
    var result = this.find({},{}, {skip: next-1, sort: { 'date_added' : -1 }}); //fetching the newest product first
    ```
  * Model.find() 메소드는 다음의 인자를 가지게 됩니다.
        * [conditions] «Object»
        * [projection] «Object|String» 반환할 선택적 필드 입니다. Query.prototype.select()를 참고하세요
        * [options] «Object» 선택입니다. 공식 문서에 다음을 확인하세요 [Query.prototype.setOptions()](https://mongoosejs.com/docs/api.html#query_Query-setOptions) 
        * [callback] «Function»
* 소팅 옵션과 스킵의 이점을 취하는 것 입니다. **skip**은 **(next-1)**까지 문서를 스킵하기 위함이고, 남은 모든 문서를 반환하는 것 입니다.

* 다음과 같이 하면, pageSize에 맞추어 문서의 특정 번호가 필요하기 때문에, **arr** 배열을 분할하고 결과 배열에 맞추어 할당하는 것 입니다.

    ```js
    arr = await result;
    let len  = arr.length;
    if(len>=pageSize){
      result = arr.slice(0, pageSize);
      if(len===pageSize){
        hasMore=false
      }
    }
    else{
      result = arr;
      hasMore=false;
    }
    ```

* 일단  if, else 조건문은 find()메소드로부터 반환된 배열의 길이가 pageSize보다 작은지에 대해 결정하게 됩니다. 만약 pageSize보다 적으면 다음 페치에 대해 더 이상 문서가 없다는 것을 나타냅니다. 결국 이는 hasMore = false 이라는 의미이죠. 두번째로 If case 경우 역시 arr.length 가 pageSzie와 동등할 때 다음 페치에 대한 문서가 더 이상 없다는 것 입니다. 이는 hasMore = false 라는 뜻 입니다.

* 다음 파트 역시 이해하는데 중요합니다. 
    ```js
    _next = next+pageSize;
    ```
* 이 코드 라인은 다음 페치를 시작하는 인덱스로부터 결정을 하는 다음의 응답 필드를 만드는 것 입니다.
* 마지막 라인은 객체를 하나 생성하고 리졸버 응답으로서의 리턴해주는 것 입니다.
    ```js
    return {
        hasMore: hasMore,
        next: _next,
        books: result
    }
    ```

### Implementing in Resolvers

* 리졸버 디렉토리 내에 쿼리폴더로 이동하여 books.js를 열고 페이지네이션에 맞추어 books리졸버를 다시 구현하시길 바랍니다.
```js
const books = async (obj, {pageSize, next})=>{
  const result = await Book.books(pageSize, next);
  if(!result){
    return null
  }
  else{
    return result
  }
}
```
*다음으로는 리졸버 엔트리 포인트, 즉 index.js 파일 내에 books 쿼리를 포인트 해주어야 합니다. 하지만 만약 쿼리 리졸버 내에 books 메소드에 쿼리하는 book를 포인트 했다느 것을 기억해야 합니다. 변한 것은 리졸버의 구현 뿐 입니다. 
* 스키마에서도 쿼리와 타입을 구현합니다
  
Playground 에서 페이징된 쿼리를 확인할 수 있습니다 .<br>

다음 장에서는 우리는 아폴로 서버에 authorization 을 다루보도록 하겠습니다.
    

