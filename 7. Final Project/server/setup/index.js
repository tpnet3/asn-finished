const mongoose = require('mongoose');
const Item = require('../models/item');
const Purchased = require('../models/purchased');
const Cart = require('../models/cart');
const Liked = require('../models/liked');
const user = require('../models/user')
const cryptoPassword = require('../lib/passwordCrypto')
var dbURI = "mongodb://deenmuhammad:+1587455Zorro@ds155352.mlab.com:55352/mern-course";  //for online db
// var dbURI = "mongodb://localhost/mern-course"
mongoose.connect(dbURI, { useNewUrlParser: true })
mongoose.connection.on('connected', ()=>{
    console.log("Connected to Database");
//     Item.insertMany([{
//         name: "Pro MERN Stack: Full Stack Web App Development with Mongo, Express, React, and Node 2nd ed. Edition",
//         author: "Vasan Subramanian",
//         pics: ["https://images-na.ssl-images-amazon.com/images/I/41osgQpU2OL._SX348_BO1,204,203,200_.jpg"],
//         price: 37.99,
//         short_desc: 'Assemble the complete stack required to build a modern web app using MongoDB, Express, React, and Node. This book also covers many other complementary tools: React Router, GraphQL, React-Bootstrap, Babel, and Webpack. This new edition will use the latest version of React (React 16) and the latest React Router (React Router 4), which has a significantly different approach to routing compared to React Router 2 which was used in the first edition of the book.',
//         long_desc: "Though the primary focus of Pro MERN Stack is to equip you with all that is required to build a full-fledged web application, a large portion of the book will be devoted to React 16. The popular MEAN (MongoDB, Express, AngularJS, Node) stack introduced Single Page Apps (SPAs) and front-end Model-View-Controller (MVC) as new and efficient paradigms. Facebook's React is a technology that competes indirectly with AngularJS. It is not a full-fledged MVC framework. It is a JavaScript library for building user interfaces (in some sense the View part). Yet, it is possible to build a web app by replacing AngularJS with React – hence the term MERN stack. Developers and architects who have prior experience in any web app stack other than the MERN stack will find the book useful to learn about this modern stack. Prior knowledge of JavaScript, HTML, and CSS is required.",
//         date_added: Date.now()
//     },
//     {
//         name: "JavaScript: The Definitive Guide: Activate Your Web Pages (Definitive Guides)",
//         author: "David Flanagan",
//         pics: ["https://images-na.ssl-images-amazon.com/images/I/51WD-F3GobL._SX379_BO1,204,203,200_.jpg", "https://i.pinimg.com/474x/23/63/e4/2363e478deb991a8b9db81fcc47093e5.jpg"],
//         price: 25.84,
//         short_desc: "Since 1996, JavaScript: The Definitive Guide has been the bible for JavaScript programmers—a programmer's guide and comprehensive reference to the core language and to the client-side JavaScript APIs defined by web browsers.",
//         long_desc: `The 6th edition covers HTML5 and ECMAScript 5. Many chapters have been completely rewritten to bring them in line with today's best web development practices. New chapters in this edition document jQuery and server side JavaScript. It's recommended for experienced programmers who want to learn the programming language of the Web, and for current JavaScript programmers who want to master it.

//         "A must-have reference for expert JavaScript programmers...well-organized and detailed."
        
//         —Brendan Eich, creator of JavaScript, CTO of Mozilla
        
//         "I made a career of what I learned from JavaScript: The Definitive Guide.”`,
//         date_added: Date.now()
//     },
//     {
//         name: "Full-Stack React Projects: Modern web development using React 16, Node, Express, and MongoDB",
//         author: "Shama Hoque",
//         pics: ["https://images-na.ssl-images-amazon.com/images/I/51YMrTUR-SL.jpg"],
//         price: 37.99,
//         short_desc: 'Unleash the power of MERN stack by building diverse web applications using React, Node.js, Express, and MongoDB',
//         long_desc: `The benefits of using a full JavaScript stack for web development are undeniable, especially when robust and widely adopted technologies such as React, Node, and Express and are available. Combining the power of React with industry-tested, server-side technologies, such as Node, Express, and MongoDB, creates a diverse array of possibilities when developing real-world web applications.

//         This book guides you through preparing the development environment for MERN stack-based web development, to creating a basic skeleton application and extending it to build four different web applications. These applications include a social media, an online marketplace, a media streaming, and a web-based game application with virtual reality features.
        
//         While learning to set up the stack and developing a diverse range of applications with this book, you will grasp the inner workings of the MERN stack, extend its capabilities for complex features, and gain actionable knowledge of how to prepare MERN-based applications to meet the growing demands of real-world web applications.`,
//         date_added: Date.now()
//     },
//     {
//         name: "React Quickly: Painless web apps with React, JSX, Redux, and GraphQL",
//         author: "Azat Mardan",
//         pics: ["https://images-na.ssl-images-amazon.com/images/I/5159foIB0EL._SX396_BO1,204,203,200_.jpg", "https://1.bp.blogspot.com/-yVSEAUl9bto/WcvuAvLkoNI/AAAAAAABDg4/svbvumL5sLcmAtFQOIU-Ar48bxIlPkqEACLcBGAs/s1600/20170927_142745.jpg"],
//         price: 47.99,
//         short_desc: `React Quickly is for anyone who wants to learn React.js fast. This hands-on book teaches you the concepts you need with lots of examples, tutorials, and a large main project that gets built throughout the book.

//         Purchase of the print book includes a free eBook in PDF, Kindle, and ePub formats from Manning Publications.`,
//         long_desc: `Successful user interfaces need to be visually interesting, fast, and flowing. The React.js JavaScript library supercharges view-heavy web applications by improving data flow between UI components. React sites update visual elements efficiently and smoothly, minimizing page reloads. React is developer friendly, with a strong ecosystem to support the dev process along the full application stack. And because it's all JavaScript, React is instantly familiar`,
//         date_added: Date.now()
//     },
//     {
//         name: "Learning React: Functional Web Development with React and Redux 1st Edition",
//         author: "Alex Banks",
//         pics: ["https://images-na.ssl-images-amazon.com/images/I/51FHuacxYjL._SX379_BO1,204,203,200_.jpg"],
//         price: 27.49,
//         short_desc: `If you want to learn how to build efficient user interfaces with React, this is your book. Authors Alex Banks and Eve Porcello show you how to create UIs with this small JavaScript library that can deftly display data changes on large-scale, data-driven websites without page reloads. Along the way, you’ll learn how to work with functional programming and the latest ECMAScript features.`,
//         long_desc: `Developed by Facebook, and used by companies including Netflix, Walmart, and The New York Times for large parts of their web interfaces, React is quickly growing in use. By learning how to build React components with this hands-on guide, you’ll fully understand how useful React can be in your organization.

//         Learn key functional programming concepts with JavaScript
//         Peek under the hood to understand how React runs in the browser
//         Create application presentation layers by mounting and composing React components
//         Use component trees to manage data and reduce the time you spend debugging applications
//         Explore React’s component lifecycle and use it to load data and improve UI performance
//         Use a routing solution for browser history, bookmarks, and other features of single-page applications
//         Learn how to structure React applications with servers in mind`,
//         date_added: Date.now()
//     },
//     {
//         name: "Fullstack React: The Complete Guide to ReactJS and Friends",
//         author: "Accomazzo Anthony (Author), Murray Nathaniel (Author), Lerner Ari (Author)",
//         pics: ["https://images-na.ssl-images-amazon.com/images/I/51mEe0St6-L._SX384_BO1,204,203,200_.jpg", "https://images-na.ssl-images-amazon.com/images/I/71BMpHmvpiL.jpg"],
//         price: 59.99,
//         short_desc: `What if you could master the entire framework in less time, with solid foundations, without beating your head against the wall? Imagine how quickly you can get all of your work done with the right tools and best practices.`,
//         long_desc: `Seriously, let's stop wasting time scouring Google, searching through incorrect, out-of-date, blog posts and get everything you need to be productive in one, well-organized place, complete with both simple and complex examples to get your app up and running.

//         You'll learn what you need to know to work professionally and build solid, well-tested, optimized apps with ReactJS. This book is your definitive guide.`,
//         date_added: Date.now()
//     },
//     {
//         name: "Pro React 1st ed. Edition",
//         author: "Cassio de Sousa Antonio",
//         pics: ["https://images-na.ssl-images-amazon.com/images/I/31SXwhHaDsL._SX348_BO1,204,203,200_.jpg"],
//         price: 32.99,
//         short_desc: `Pro React teaches you how to successfully structure increasingly complex front-end applications and interfaces. This book explores the React library in depth, as well as detailing additional tools and libraries in the React ecosystem, enabling you to create complete, complex applications.`,
//         long_desc: `You will learn how to use React completely, and learn best practices for creating interfaces in a composable way. You will also cover additional tools and libraries in the React ecosystem (such as React Router and Flux architecture). Each topic is covered clearly and concisely and is packed with the details you need to learn to be truly effective. The most important features are given no-nonsense, in-depth treatment, and every chapter details common problems and how to avoid them.

//         If you already have experience creating front-end apps using jQuery or perhaps other JavaScript frameworks, but need to solve the increasingly common problem of structuring complex front-end applications, then this book is for you. Start working with React like a pro - add Pro React to your library today.`,
//         date_added: Date.now()
//     },
// ]);
Item({
        name: "Beginning React: Simplify your frontend development workflow and enhance the user experience of your applications with React",
        author: "Andrea Chiarelli",
        pics: ["https://images-na.ssl-images-amazon.com/images/I/510uP2rVKUL._SX404_BO1,204,203,200_.jpg", "https://images-na.ssl-images-amazon.com/images/I/61w6IJfexxL.jpg", "https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://coursera-course-photos.s3.amazonaws.com/5f/506890537e11e8966b999b6cf9ce61/logo.png?auto=format,compress", "https://i2.wp.com/i.imgur.com/l7YL4qn.png?resize=400%2C400&quality=100&ssl=1", "https://images-na.ssl-images-amazon.com/images/I/91a48Ua65IL._SY600_.jpg"],
        price: 14.99,
        short_desc: `Key Features: 
        Elaborately explains basics before introducing advanced topics
        Explains creating and managing the state of components across applications
        Implement over 15 practical activities and exercises across 11 topics to reinforce your learning`,
        long_desc: `Projects like Angular and React are rapidly changing how development teams build and deploy web applications to production. In this book, you’ll learn the basics you need to get up and running with React and tackle real-world projects and challenges. It includes helpful guidance on how to consider key user requirements within the development process, and also shows you how to work with advanced concepts such as state management, data-binding, routing, and the popular component markup that is JSX. As you complete the included examples, you’ll find yourself well-equipped to move onto a real-world personal or professional frontend project.`,
        date_added: Date.now()
}).save()
});
mongoose.connection.on('error', ()=>{
    throw new Error("Failed to Connect to Databse");
});
mongoose.connection.on('disconnected', ()=>{
    throw new Error("Databse Disconnected");
});