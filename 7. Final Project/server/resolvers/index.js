const item = require('./item');
const user = require('./user');
const cart = require('./cart');
const liked = require('./liked');
const purchase = require('./purchase');

module.exports = {
  Query: {
    getItem: item.getItem,
    getItemsBatch: item.getItemsBatch,
    logInWithPassword: user.loginWithPassword,
    getCartItems: cart.getCartItems,
    getLikedItems: liked.getLikedItems,
    getPurchasedItems: purchase.getPurchasedItems,
  },
  
  Mutation: {
    signUpWithPassword: user.signUpWithPassword,
    addToCart: cart.addToCart,
    removeItemFromCart: cart.removeItemFromCart,
    likeItem: liked.likeItem,
    removeItemFromLiked: liked.removeItemFromLiked,
    purchaseItem: purchase.purchaseItem,
    purchaseItemsAsBatch: purchase.purchaseItemsAsBatch,
    removeItemFromPurchased: purchase.removeItemFromPurchased,
    modifyUserAccount: user.modifyUserAccount,
    deleteAccount: user.deleteAccount
  }
}