const Item = require('../models/item')

const getItem = async (obj, {_id}, ctx)=>{
  const item = await Item.getItem(_id, ctx);
  if(!item){
    return null;
  }
  return item;
}

const getItemsBatch = async (obj, {pageSize, next}, ctx)=>{
  const result = await Item.getItemsBatch(pageSize, next);
  if(result===null){
    return null;
  }
  return result;
}
// const getDisapprovedProductBatch = async (obj, {pageSize, next}, ctx)=>{
//     const result = await products.getDisapprovedProductBatch(pageSize, next);
//     if(!result){
//         return null;
//     }
//     return result;
// }
// const getApprovedProductBatch = async (obj, {pageSize, next}, ctx)=>{
//     const result = await products.getApprovedProductBatch(pageSize, next);
//     if(!result){
//         return null;
//     }
//     return result;
// }
// const getApprovedProductBatchAsAdmin = async (obj, {pageSize, next}, ctx)=>{
//     const result = await products.getApprovedProductBatchAsAdmin({pageSize, next}, ctx);
//     if(!result){
//         return null;
//     }
//     return result;
// }
// const getDisapprovedProductBatchAsAdmin = async (obj, {pageSize, next}, ctx)=>{
//     const result = await products.getDisapprovedProductBatchAsAdmin({pageSize, next}, ctx);
//     if(!result){
//         return null;
//     }
//     return result;
// }
// const getPendingProductBatchAsAdmin = async (obj, {pageSize, next}, ctx)=>{
//     const result = await products.getPendingProductBatchAsAdmin({pageSize, next}, ctx);
//     if(!result){
//         return null;
//     }
//     return result;
// }
// const getPendingProductBatch = async (obj, {pageSize, next}, ctx)=>{
//     const result = await products.getPendingProductBatch(pageSize, next);
//     if(!result){
//         return null;
//     }
//     return result;
// }
// const getSaleProductBatch = async (obj, {pageSize, next}, ctx)=>{
//     const result = await products.getSaleProductBatch(pageSize, next);
//     if(!result){
//         return null;
//     }
//     return result;
// }

module.exports = {
  getItem,
  getItemsBatch,
  // getSaleProductBatch,
  // getDisapprovedProductBatch,
  // getPendingProductBatch,
  // getApprovedProductBatchAsAdmin,
  // getDisapprovedProductBatchAsAdmin,
  // getPendingProductBatchAsAdmin,
  // getApprovedProductBatch
}