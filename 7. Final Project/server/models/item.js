const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
  name: String,
  author: String,
  pics:[
    String
  ],
  price: Number,
  short_desc: String,
  long_desc: String,
  date_added: Number,
})

itemSchema.statics.getItem = async function (_id, ctx) {
  if(ctx.headers.accessToken===null){//user not logged in so that we do not need to care if the item is liked or added to cart, so like and inCart shall be returned as false
    const item = await this.findById({_id: _id}).exec();
    if(item!==null){
      let response = {
        _id: item._id,
        name: item.name,
        author: item.author,
        pics:item.pics,
        price: item.price,
        short_desc: item.short_desc,
        long_desc: item.long_desc,
        date_added: item.date_added,
        liked: false,
        inCart: false
      }
      return response;
    }
    return item
  }
  else{
    const user = await this.model('User').verifyUser(ctx.headers.accessToken);
    if(!user){
      throw new Error(`tokenFailed`)
    }
    const item = await this.findById({_id: _id}).exec();
    if(item!==null){
      let inCart = false;
      let liked = false;
      if((await this.model('Cart').inCart(_id, user._id))){
        inCart = true;
      }
      if((await this.model('Liked').inLiked(_id, user._id))){
        liked = true;
      }
      let response = {
        _id: item._id,
        name: item.name,
        author: item.author,
        pics:item.pics,
        price: item.price,
        short_desc: item.short_desc,
        long_desc: item.long_desc,
        date_added: item.date_added,
        liked: liked,
        inCart: inCart
      }
      return response;
    }
    else{
      return item
    }
  }
}

itemSchema.statics.getItemsBatch = async function (pageSize, next) {
  var hasMore=true;
  var _next;
  var arr = [];
  var result = this.find({},{}, {skip: next-1, sort: { 'date_added' : -1 }}); //fetching the newest product first
  arr = await result;
  let len  = arr.length;
  if(len>=pageSize){
    result = arr.slice(0, pageSize);
    if(len===pageSize){
      hasMore=false
    }
  }
  else{
    result = arr;
    hasMore=false;
  }
  _next = next+pageSize;
  return {
    hasMore: hasMore,
    next: _next,
    items: result
  }
}

module.exports = mongoose.model('Item', itemSchema);