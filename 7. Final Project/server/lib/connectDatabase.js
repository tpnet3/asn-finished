const mongoose = require('mongoose');

var dbURI = "mongodb://deenmuhammad:+1587455Zorro@ds155352.mlab.com:55352/mern-course";  //for online db
// var dbURI = "mongodb://localhost/mern-course"; //using IP adress

mongoose.connect(dbURI, { useNewUrlParser: true })

mongoose.connection.on('connected', ()=>{
  console.log("Connected to Database");
});

mongoose.connection.on('error', (err)=>{
  throw new Error("Failed to Connect to Database "+err);
});

mongoose.connection.on('disconnected', (err)=>{
  throw new Error("Database Disconnected "+err);
});