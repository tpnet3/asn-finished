import React from 'react'
import {Link} from 'react-router-dom'
import './header.css'

class Header extends React.Component {
  constructor(){
    super();
    this.state={
      loggedIn: false,
      hasScrolled: false,
    } 
  }
  componentWillMount(){
    if(localStorage.getItem('accessToken')!==''){
      this.setState({
        loggedIn: true
      })
    }
  }
  componentDidMount = () => {
    this.handleScroll();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.handleScroll);
  }
  handleScroll = (event) => {
    const scrollTop = window.pageYOffset;

    if (scrollTop > 20 ) {
      this.setState({ hasScrolled: true })
    } else {
      this.setState({ hasScrolled: false})
    }
  }
  _handleNotLoggedUser = (link)=>{
    if(!this.state.loggedIn){
      alert(`Plase login before you access ${link}`)
    }
  }
  render() {
    return (
      <div className={this.state.hasScrolled?"HeaderScrolled":"Header"}>
      <div className="LogoWrapper">
      <Link to="/"><img className="Logo" width="55px" height="45px" alt="pic" src={require('./../images/logo.png')}/></Link>
      </div>
        <div className="HeaderGroup">
          <Link to="/" ><button className={this.props.onFocus===1?"ButtonOnFocus":"Button"}>Home</button></Link>
          <Link onClick={()=>this._handleNotLoggedUser("Cart")} to={this.state.loggedIn?"/cart":"/login"}><button className={this.props.onFocus===2?"ButtonOnFocus":"Button"}>Cart</button></Link>
          <Link onClick={()=>this._handleNotLoggedUser("Profile")} to={this.state.loggedIn?"/profile":"/login"}><button className={this.props.onFocus===3?"ButtonOnFocus":"Button"}>Profile</button></Link>
          {this.state.loggedIn?
          <Link onClick={()=>{alert("You will be logged out!"); localStorage.setItem('accessToken', ''); this.props.history.push('/');}} to="/"><button className="LogoutButton">Logout</button></Link>:
          <Link to="/login"><button className="LogoutButton">Login</button></Link> 
          }
        </div>
      </div>
    )
  }
}

export default Header