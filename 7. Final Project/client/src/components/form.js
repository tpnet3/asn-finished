import React, { Component } from "react";
import styled from 'styled-components'
import Button from './button'
import {Link} from 'react-router-dom'

const Container = styled.div`
  width: 500px;
  height: 500px;
  position: relative;
  overflow: hidden;
  border-radius: 20px;
  box-shadow: 0 20px 40px rgba(0,0,0,0.25);
  display: grid;
  background: #010f2b;
  justify-content: center;
  margin: 0 auto;
  grid-template-rows: 1fr 1fr;
  transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
  cursor: pointer;
  .link{
    color: white;
    text-align: center;
    font-size: 12px;
    margin: 10px;
  }
  @media screen and (max-width: 720px) {
    width: 350px;
    height: 450px;
    }
`

const Label = styled.label `
  font-size: 30px;
  color: #C6D0EB;
  font-weight: bold;
  margin-bottom: 10px;
  display: block;
  @media screen and (max-width: 720px) {
    font-size: 20px;
  }
`
const Input = styled.input `
  background: rgba(255, 255, 255, 0.15);
  border: 1px solid rgba(255, 255, 255, 0.15);
  box-sizing: border-box;
  backdrop-filter: blur(40px);
  border-radius: 14px;
  height: 60px;
  width: 400px;
  color: white;
  font-size: 24px;
  padding: 18px 20px;
  ::placeholder {
    font-size: 24px;
  }
  @media screen and (max-width: 720px) {
    height: 40px;
    width: 300px;
    font-size: 18px;
  }
  ::placeholder {
    font-size: 18px;
  }
`
const Field = styled.div `
  margin-top: 55px;
  margin-bottom: 0 !important; 
  p{
    color: red;
    font-size: 10px;
  }
`

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      empty: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    if(this.state.username===''){
      this.setState({empty: 'username'})
      return
    }
    if(this.state.password===''){
      this.setState({empty: 'password'})
      return
    }
    
    this.props.commitSubmit(this.state.username, this.state.password)
  }

  render() {

    const { BtnName, TextLabel, PassLabel, } = this.props;

    return (
      <Container>
        <form className="form" onSubmit={this.handleSubmit}>
          <Field>
            <Label className="Label">{TextLabel}</Label>
            <Input
              className="input"
              type="text"
              name="username"
              placeholder="Username"
              value={this.state.username}
              onChange={this.handleChange}
            />
            {this.props.usernameTaken?<p>Username already exists</p>:null}
            <p>{this.props.usernameWrong?'Incorrect Username':''}</p>
            {this.state.empty==='username'?<p>Please fill username field</p>:null}
          </Field>

          <Field>
            <Label className="Label">{PassLabel}</Label>
            <Input
              className="input"
              type="password"
              name="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleChange}
            />
            {this.state.empty==='password'?<p>Please fill password field</p>:null}
            <p>{this.props.passwordWrong?'Incorrect Password':''}</p>
          </Field>

          <Button
            background="#56CCF2"
            fontSize="24px"
            borderRadius="12px"
            mt="55px"
            color="white"
            width="150px"
            height="45px">

            {BtnName}</Button>
        </form>
        {this.props.BtnName==='SignUp'?
        <Link className='link' to='/login'>Already signed Up? Click here</Link>:
        this.props.BtnName==='Login'?
        <Link className='link' to='/signup'>Need to Sign Up? Click here</Link>:
        null
        }
      </Container>
    );
  }
}

export default Form;
