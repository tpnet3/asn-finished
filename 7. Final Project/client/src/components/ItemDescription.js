import React from 'react'
import styled from 'styled-components'



const SectionTitleGroup = styled.div`
    display: block;
    margin: 0 40px;

    @media (max-width: 720px) {
        grid-template-columns: 1fr;
    }
`

const SectionName = styled.h3`
    color: black;
    font-size: 40px;
    padding: 0;
    margin: 0;
    line-height: 1.2;

    @media (max-width: 720px) {
        font-size: 30px;
        margin: 20px 0;
    }
`

const SectionAuthor = styled.h5`
    color: black;
    margin: 20px 0;
    font-style: italic;
    font-size: 24px;
    padding: 0;
    margin: 0;
    line-height: 1.2;

    @media (max-width: 720px) {
        font-size: 20px;
        color: rgb(100,100,100);
    }
`
const SectionPrice = styled.div`
    margin: 20px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
`
const CardPrice = styled.h4 `
    color: rgba(100,100,100, 0.5);
    text-decoration: line-through;
    text-transform: uppercase;
    font-weight:600;
    font-size:18px;
    align-self: end;
    margin: 5px 0 0 0;
`
const CardActualPrice = styled.h3 `
    color: rgba(100,205,100, 1);
    text-transform: uppercase;
    font-weight:600;
    font-size: 25px;
    align-self: end;
    margin: 0 0 10px 20px;
`

const SectionText = styled.p`
    color: black;
`

const Section = props => (

		<SectionTitleGroup>
			<SectionName>{props.bookname}</SectionName>
            <SectionAuthor>by {props.author}</SectionAuthor>
            <SectionPrice>
                <CardPrice>{(props.price+props.price*props.discount).toFixed(2)}$</CardPrice>
                <CardActualPrice>👉{props.price}$</CardActualPrice>
            </SectionPrice>
			<SectionText><h4>In Brief: </h4>{props.short_desc}</SectionText>
            <SectionText><h4>Description: </h4>{props.long_desc}</SectionText>
		</SectionTitleGroup>

)

export default Section