import styled from 'styled-components'

const Button = styled.button`
    /* background: linear-gradient(102.24deg, #9B51E0 0%, #3436E7 100%); */
    background: ${props => props.background};
    box-shadow: 0px 10px 20px rgba(101, 41, 255, 0.15);
    border-radius:${props=> props.borderRadius};

    /* margins */
    margin: ${props=> props.m};
    margin-top: ${props=> props.mt};
    margin-bottom: ${props=> props.mb};
    margin-left: ${props=> props.ml};
    margin-right: ${props=> props.mr};
    
    /* paddings */
    padding-left: ${props=> props.pl};
    color: ${props=> props.color};
    border: none;
    min-width: ${props=> props.width};
    min-height: ${props=> props.height};
    font-weight: 600;
    font-size: ${props=> props.fontSize};
    justify-self: center;
    font-weight: bold;
    transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
    &:hover {
        box-shadow: 0 20px 40px rgba(0,0,0, 0.15);
        transform: translateY(-3px);
    }
`

export default Button;