import styled from 'styled-components'
import React, { Component } from 'react'
import {Mutation} from 'react-apollo'
import {ADD_TO_CART} from '../lib/gql/cart'
import {REMOVE_FROM_CART} from '../lib/gql/cart'


const Button = styled.button`
    /* background: linear-gradient(102.24deg, #9B51E0 0%, #3436E7 100%); */
    background: linear-gradient(102.24deg, #9B51E0 0%, #3436E7 100%);
    box-shadow: 0px 10px 20px rgba(101, 41, 255, 0.15);
    border-radius: 30px;
    
    /* paddings */
    color: white;
    border: none;
    min-width: 100px;
    min-height: 40px;
    font-weight: 600;
    font-size: 10px;
    justify-self: center;
    font-weight: bold;
    transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
    &:hover {
        box-shadow: 0 20px 40px rgba(0,0,0, 0.15);
        transform: translateY(-3px);
    }
`

export default class CartButton extends Component {
    constructor(){
        super()
        this.state={
            inCart: false,
            loggedIn: false
        }
    }
    componentWillMount(){
        this.setState({inCart: this.props.inCart})
        if(localStorage.getItem('accessToken')!==''){
            this.setState({
                loggedIn: true
            })
        }
    }
  render() {
    return (
        this.state.inCart?
        <Mutation mutation={REMOVE_FROM_CART}
        onCompleted={data => {
        if (data.removeItemFromCart) {
            this.setState({
                inCart: false
            });
        }
      }}
      onError={error => {
          console.log(error)
      }}
      >
      {(removeItemFromCart, { data, error }) => (
          <Button 
            onClick={()=>{
                removeItemFromCart({ variables: { item_id: this.props.item_id }});
            }}>Remove from Cart</Button>
      )
      }
        </Mutation>:
        <Mutation mutation={ADD_TO_CART}
        onCompleted={data => {
        if (data.addToCart) {
            this.setState({
                inCart: true
            });
        }
      }}
      onError={error => {
          console.log(error)
      }}
      >
      {(addToCart, { data, error }) => (
          <Button 
            onClick={()=>{
                if(this.state.loggedIn){
                addToCart({ variables: { item_id: this.props.item_id }});
                }
                else{
                    this.props.pushHistory()
                }
            }}>Add to Cart</Button>
      )
      }
        </Mutation>
    )
  }
}
