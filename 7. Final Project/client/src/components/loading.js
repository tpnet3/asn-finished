import React, { Component } from 'react'
import styled from 'styled-components'


const LoadingWrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    img{
        width: 200px;
        height: auto;
    }
`

export default class Loading extends Component {
  render() {
    return (
      <LoadingWrapper>
          <img alt="loading" src={require('./../images/book_loading.gif')}/>
      </LoadingWrapper>
    )
  }
}
