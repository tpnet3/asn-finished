import React from 'react'
import styled from 'styled-components'
import {Link} from 'react-router-dom'

const Card = styled.div `
    width: 200px;
    height: 300px;
    position: relative;
    overflow: hidden;
    border-radius: 0 5px 5px 0;
    box-shadow: 0 10px 20px rgba(0,0,0,0.25);
    transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
    cursor: pointer;

    &:hover {
        transform: scale(1.1, 1.1);
        box-shadow: 0 20px 40px rgba(0,0,0,0.5);
    }
`
const CardImage = styled.img ` 
    width: 100%;
    height: 100%;
`

const CardTitle = styled.div `
    color: black;
    font-size: 16px;
    margin: 20px 0 0 20px;
    width: 190px;
`

const CardPrice = styled.p `
    color: rgba(100,100,100, 0.5);
    text-decoration: line-through;
    text-transform: uppercase;
    font-weight:600;
    font-size:18px;
    align-self: end;
    margin: 0 0 20px 20px;
`
const CardActualPrice = styled.p `
    color: rgba(100,205,100, 1);
    text-transform: uppercase;
    font-weight:600;
    font-size:18px;
    align-self: end;
    margin: 0 0 20px 20px;
`

const Cardgroup = props => (
    <Link to={{
        pathname: props.to+`/${props.data}`,
      }}>
    <Card>
        <CardImage src={props.images?props.images: "http://helideckinspect.com/v4/wp-content/uploads/2018/01/no_image.png"} />
    </Card>
    <CardTitle>{props.title}</CardTitle>
    <CardPrice>{(props.price+props.price*props.discount).toFixed(2)}$</CardPrice>
    <CardActualPrice>👉{props.price}$</CardActualPrice>
    </Link>
) 

export default Cardgroup
