import React, { Component } from 'react'
import styled from 'styled-components'
import Form from './form'
import {Mutation} from 'react-apollo'
import {DELETE_ACCOUNT} from './../lib/gql/users'
 
export default class Modal extends Component {
  constructor(){
    super();
    this.state ={
      passwordWrong: false
    }
  }
  render() {
    const ModalWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    position: fixed;
    top: 0;
    left: 0;
    width: ${this.props.width};
    transition: 0.8s cubic-bezier(0.2, 0.8, 0.2, 1);
    height: ${this.props.height};
    background-color: rgba(250,250,250,.8);
    z-index: 0;
`
    return (
      <Mutation mutation={DELETE_ACCOUNT}
      onCompleted={()=>{
        localStorage.setItem('accessToken', '')
        alert("Account Deleted! Thank you for using our service!")
        this.props.commitSubmit();
      }}
      onError={(error)=>{
        if(error.message==='GraphQL error: passwordIncorrect'){
          this.setState({passwordWrong: true})
        }
        else{
          alert(error)
        }
      }}
      >
      {(deleteAccount, { data, error }) => (
      <ModalWrapper>
        <Form
            TextLabel="User Name"
            PassLabel="Password"
            BtnName="Proceed to Delete"
            passwordWrong = {this.state.passwordWrong}
            commitSubmit={(username, password)=>{
              deleteAccount({variables: {password: password}})
            }}
           />
           <p onClick={()=>{this.props.toggleModal()}}>Click here to Close</p>
      </ModalWrapper>
      )}
      </Mutation>
    )
  }
}
