import gql from "graphql-tag";

export const GET_ITEMS_BATCH = gql`
  query getItemsBatch($pageSize: Int!, $next: Int!){
    getItemsBatch(pageSize: $pageSize, next: $next){
        hasMore
        next
        items{
            _id
            pics
            name
            price
        }
    }
  }
`;

export const GET_ITEM = gql`
    query getItem($_id: String!){
        getItem(_id: $_id){
            name
            author
            price
            pics
            short_desc
            long_desc
            inCart
            liked
        }
    }
`

export const GET_CART_AND_LIKED_ITEMS = gql`
    query getCartItems{
        getCartItems{
            item_id{
            _id
            name
            pics
            price
            }
        }
        getLikedItems{
            item_id{
            _id
            name
            pics
            price
            }
  }
    }
`