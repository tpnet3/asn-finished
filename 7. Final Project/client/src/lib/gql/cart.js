import gql from 'graphql-tag'

export const ADD_TO_CART = gql`
    mutation addToCart($item_id: String!){
        addToCart(item_id: $item_id)
    }
`
export const REMOVE_FROM_CART = gql`
    mutation removeItemFromCart($item_id: String!){
        removeItemFromCart(item_id: $item_id)
    }
`

