import React, { Component } from 'react';
import Card from '../components/card'
import Loading from './../components/loading'
import Error from './../components/error'
import Layout from '../containers/layout'
import Header from '../containers/header'
import Footer from '../containers/footer'
import { Query } from "react-apollo";
import {GET_CART_AND_LIKED_ITEMS}  from './../lib/gql/items'

class Items extends Component {
  render() {
    return (
      <Query query={GET_CART_AND_LIKED_ITEMS}>
      {({ loading, error, data }) => {
      if (loading) return <Loading />;
      if (error) {return <Error>{error.message}</Error>;}
        console.log(data)
      return(
      <Layout>
        <Header onFocus={2} />
        <div className="Cards">
          <h2>Items in Your Cart</h2>
          <div className="CardGroup">
          {data.getCartItems.length>0?null:<p>Cart empty</p>}
          {data.getCartItems.map((getCartItems)=>{
            return (
              <Card
              title={getCartItems.item_id.name}
              images={getCartItems.item_id.pics.length>0?getCartItems.item_id.pics[0]:[]}
              price={getCartItems.item_id.price}
              discount={0.1}
              to = "item"
              data={getCartItems.item_id._id}
            />
            )
          })}
          </div>
          <h2>Items You Liked</h2>
          <div className="CardGroup">
          {data.getLikedItems.length>0?null:<p>No Liked items</p>}
          {data.getLikedItems.map((getLikedItems)=>{
            return (
              <Card
              title={getLikedItems.item_id.name}
              images={getLikedItems.item_id.pics.length>0?getLikedItems.item_id.pics[0]:[]}
              price={getLikedItems.item_id.price}
              discount={0.1}
              to = "item"
              data={getLikedItems.item_id._id}
            />
            )
          })}
          </div>
        </div>
        <Footer />
      </Layout>
      )}}
      </Query>
    );
  }
}

export default Items;
