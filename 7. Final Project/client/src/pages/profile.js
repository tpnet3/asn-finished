import React, { Component } from 'react';
import Layout from '../containers/layout'
import Header from '../containers/header'
import Footer from '../containers/footer'
import ProfileForm from '../components/profileForm'
import Modal from './../components/modal'
import { Mutation } from "react-apollo";
import {MODIFY_USER_ACCOUNT} from './../lib/gql/users'

class Profile extends Component {
  constructor(){
    super();
    this.state = {
      modalOpen: false,
      usernameTaken: false,
      oldPasswordIncorrect: false
    }
  }
  render() {
    return (
      <Mutation mutation={MODIFY_USER_ACCOUNT}
      onCompleted={data => {
          alert("Account Modified")
          window.location.reload();
      }}
      onError={error => {
        if(error.message==='GraphQL error: usernameTaken'){
          this.setState({usernameTaken: true, oldPasswordIncorrect: false})
        }
        if(error.message==='GraphQL error: oldPasswordIncorrect'){
          this.setState({oldPasswordIncorrect: true, usernameTaken: false})
        }
        else{
          alert(error)
        }
      }}
      >
      {(modifyUserAccount, { data, error }) => (
        
      <Layout>
        <Header onFocus={3} />
        <div className="SingleItem">
          {/* <img width="200px" height="200px" src={"https://cdn4.iconfinder.com/data/icons/linecon/512/photo-512.png"} /> */}
          {/* <h2>Change your profile Information</h2> */}
          <div className="Profile">
           <ProfileForm
            TextLabel="New User Name"
            PassLabel="Old Password"
            PassLabel2="New Password"
            BtnName="Change"
            BtnName2="Delete Profile"
            usernameTaken = {this.state.usernameTaken}
            oldPasswordIncorrect = {this.state.oldPasswordIncorrect}
            commitSubmit={(new_username, old_password, new_password)=>{
              modifyUserAccount({ variables: { new_username: new_username, old_password: old_password, new_password: new_password }});
            }}
            toggleModal = {()=>{this.setState({modalOpen: !this.state.modalOpen})}}
           />
          </div>
          {
            this.state.modalOpen?
              <Modal 
                width = {this.state.modalOpen?'100%':'0'}
                height = {this.state.modalOpen?'100%':'0'}
                toggleModal = {()=>{this.setState({modalOpen: !this.state.modalOpen})}}
                commitSubmit={()=>{this.props.history.push('/')}}
              />:
              null
          }
        </div>
        <Footer/>
      </Layout>
      )
      }
    </Mutation>
    );
  }
}

export default Profile;



