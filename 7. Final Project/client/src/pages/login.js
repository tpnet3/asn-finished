import React, { Component } from 'react';
import Layout from '../containers/layout'
import Header from './../containers/header'
import Footer from './../containers/footer'
import ProfileForm from '../components/form'
import { ApolloConsumer } from 'react-apollo';
import {LOG_IN_WITH_PASSWORD} from './../lib/gql/users'


class Login extends Component {
  constructor(){
    super();
    this.state = {
      error: ''
    }
  }
  render() {
    return (
      <ApolloConsumer>
        {client => (
          <Layout>
            <Header />
            <div className="SingleItem">
              <h2>Login</h2>
          
              <div className="Profile">
              <ProfileForm
                TextLabel="User Name"
                PassLabel="Password"
                usernameWrong = {this.state.error==='userNotFound'?true:false}
                passwordWrong = {this.state.error==='passwordIncorrect'?true:false}
                BtnName="Login"
                commitSubmit={async (username, password) => {
                  client.query({
                    query: LOG_IN_WITH_PASSWORD,
                    variables: {username: username, password: password}
                  }).then(({ data }) => {
                    localStorage.setItem('accessToken', data.logInWithPassword);
                    this.props.history.push('/');
                  }).catch(({ graphQLErrors }) => {
                    this.setState({error: graphQLErrors[0].message})
                    })
                }}
              />
              </div>
            </div>
            <Footer />
          </Layout>
        )}
      </ApolloConsumer>
    );
  }
}

export default Login;