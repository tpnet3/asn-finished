import React from "react";

class App extends React.Component {
  state = {
    name: " "
  };

  onFormSubmit = evt => {
    evt.preventDefault();
    this.setState({ name: evt.target.value });
  };

  render() {
    return (
      <div>
        <h1>Sign Up Sheet</h1>

        <form onSubmit={this.onFormSubmit}>
          <input
            type="text"
            placeholder="Name"
            value={this.state.name}
            onChange={this.onFormSubmit}
          />

          <input type="submit" />
        </form>
        {this.state.name.length > 6 ? "Validated" : "More than 6 letters"}
      </div>
    );
  }
}

export default App;
