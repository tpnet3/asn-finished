# Controlled vs Uncontrolled forms

폼은 어플리케이션에서 중요한 파트 중 하나입니다. 우리는 유저의 정보를 추가하거나, 수정하거나, 결과를 검색하거나, 사진을 업로드할 때 폼을 사용합니다. 간단하게는 `form` 태그 안에서 `input` 이나 `submit` 태그를 사용해 구현할 수 있습니다. 하지만, 좀 더 멋진 웹사이트와 상호작용을 만들기 위해서는 어느정도의 프로그래밍이 필요합니다.

리액트에서의 폼은 일반적인 자바스크립트에서 사용하는 방법과 조금 다릅니다. 리액트에서 폼을 다룰 때에는 두가지 방법이 있고, 바로 **uncontrolled** 와 **controlled** 방법입니다. 이 것이 무것을 의미하는지 하나씩 살펴보겠습니다.

## 언제 사용하나요?

**Uncontrolled** 최소한의 기능으로 매우 기본적인 경우에 유용합니다.

**Controlled** 사용자의 값을 검증하거나 사용자에게 화면상 인터렉션을 발생시킬 때 유용합니다.

## Uncontrolled

관리되지 않는 폼은 우리가 알고있는 일반적인 폼과 비슷합니다. 사용자의 입력값들은 브라우저가 알아서 관리하고, 우리는 단지 사용자가 submit 을 요청 했을 때 입력된 값을 사용하면 됩니다.

```js

onFormSubmit = evt => {
    evt.preventDefault();
    console.log(this.refs.name.value);
  };

  render() {
    return (
      <div>
        <h1>Sign Up Sheet</h1>

        <form onSubmit={this.onFormSubmit}>
          <input type="textField" placeholder="Name" ref="name" />

          <input type="submit" />
        </form>
      </div>
    );
  }

```

> 이 예제를 **`src/examples/uncontrolled.js`** 에서 확인할 수 있습니다.

위 코드를 보면, 우리는 `refs` (레퍼런스) 를 사용했습니다. 이 것은 리액트에서 DOM 의 정보를 가져와 설정할 수 있도록 도와주는 규칙입니다. 만약 컴포넌트에서 `ref` 라는 속성 값으로 키를 설정하면, this.refs 에 DOM 이 할당됩니다. 즉, 위 코드와 같이 `ref="name"` 으로 설정했다면, `this.refs.name` 으로 접근할 수 있게 됩니다.

그래서 만약 사용자가 submit 버튼을 눌렀다면, `form` 태그의 `onSubmit` 속성값인 메소드 `this.onFormSubmit` 가 실행되고, 메소드 내에서 `this.refs.name.value` 를 통해 DOM 엘리먼트에 사용자가 입력한 값을 가져올 수 있습니다.

```js
onFormSubmit = evt => {
  evt.preventDefault();
  console.log(this.refs.name.value);
};
```

> **`preventDefault()`** 는 브라우저의 기본 설정값으로 인해 다른 페이지로 넘어가지 않도록 하는 것입니다.

![](https://cl.ly/475fadb3a0da/download/Screen%252520Shot%2525202019-03-17%252520at%2525208.00.56%252520AM.png)

> 임베디드 브라우저를 새 창으로 열어서 개발자 도구를 사용하면, 결과 값을 확인할 수 있습니다.

이 값은 사용자가 제출했을때 마지막 값을 가져옵니다.

### Controlled

제어되는 인풋에서는 사용자가 입력한 값들을 상태값으로 저장하고, 변경될 때마다 매번 렌더링합니다. 사용자의 입력값에 따라 화면을 제어해야할 때 유용합니다. 예를 들어, 사용자가 이메일이 아닌 글자를 이메일 입력칸에 입력했을 때, 이메일의 형식을 입력하라고 알림 메시지를 띄워줄 수 있습니다.

```js
state = {
    name: " "
  };

  onFormSubmit = evt => {
    evt.preventDefault();
    this.setState({ name: evt.target.value });
  };

  render() {
    return (
      <div>
        <h1>Sign Up Sheet</h1>

        <form onSubmit={this.onFormSubmit}>
          <input
            type="text"
            placeholder="Name"
            value={this.state.name}
            onChange={this.onFormSubmit}
          />
          
          <input type="submit" />
        </form>
      </div>
    );
  }
```

여기 있는 리액트 컴포넌트는 `input` 필드의 값에 따라 상태 값을 변경하고 있습니다. `onChange` 속성은 상태값이 변할때마다 호출됩니다.

![pic](https://cl.ly/7a074fed4eb5/download/controlled.gif)

Above example shows the difference of uncontrolled and controlled inputs.

## 도전 과제

`src/example/controlled.js` 파일과 `src/example/uncontrolled.js` 파일을 확인하고 `App.js` 파일에서 주석을 제거하여 소스코드가 잘 실행되는지 확인하고 어떤 차이점이 있는지 살펴보세요.