import React, { Component } from "react";
import basicValidation from "./example/basicValidation";
import controlled from "./example/controlled";
import uncontrolled from "./example/uncontrolled";

class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>

          <h3>Below challange codes</h3>
          {/*<basicValidation />*/}
          {/*<controlled />*/}
          {/*<uncontrolled />*/}
        </center>
      </div>
    );
  }
}

export default App;
