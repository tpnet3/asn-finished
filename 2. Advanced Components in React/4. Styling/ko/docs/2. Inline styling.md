# Inline Styling

리액트에서 인라인 스타일링은 일반적인 CSS 와 약간 다릅니다. 객체를 사용해야합니다. CSS 에서 사용하는 키와 같지만, `camelCase` 를 사용한다는 점만 다릅니다. 예를 들어, `font-size` 는 객체의 키값으로 `fontSize` 라소 작성해야합니다. 객체의 값은 보통 문자열로 지정하면 됩니다.

```js
import React from "react";

const divStyle = {
  margin: "40px",
  border: "5px solid pink"
};
const pStyle = {
  fontSize: "15px",
  textAlign: "center"
};

const Box = () => (
  <div style={divStyle}>
    <p style={pStyle}>Get started with inline style</p>
  </div>
);

export default Box;
```

위 코드는 스타일 객체를 만들고, style 속성값으로 그 객체를 전달해줬습니다. 이렇게 DOM 에 스타일을 지정할 수 있습니다.

다른 방법도 있습니다.

```js
import React from "react";

const Box = () => (
  <div style={{ margin: "40px", border: "5px solid pink" }}>
    <p style={{ fontSize: "15px", textAlign: "center" }}>
      Get started with inline style
    </p>
  </div>
);

export default Box;
```

이 코드는 똑같은 코드이지만, 객체를 따로 변수에 저장하지 않고 JSX 내에서 직접 지정해줬습니다. 이렇게 작성할수도 있습니다.

## 도전 과제

`src/challenge/inlineStyle/inlineStyle.js` 파일에는 `externalStyle` 에서 만든 것과 똑같은 `Button` 컴포넌트가 들어있습니다. 이 컴포넌트를 아래 CSS 스타일과 똑같은 동작을 하도록 이번 시간에 배운 인라인 스타일을 활용해 만들어보세요.

```css
.myButton {
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
}
```