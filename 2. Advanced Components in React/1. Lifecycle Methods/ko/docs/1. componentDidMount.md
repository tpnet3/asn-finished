# componentDidMount

**`Intro to React`** 코스에서 **`Component Lifecycle`** 에 대해 조금 소개했었습니다.

`componentDidMount` 메소드는 **`render`** 메소드가 실행되고 반환되는 JSX를 DOM (화면) 으로 렌더링 시킨 이후에 호출됩니다. 그리고 Reactjs.org 공식 문서에서 `componentDidMount` 는 API 를 호출할때 사용하기에 가장 좋은 방법이라고 설명하고 있습니다.

이 메소드안에서 **`setState()`** 메소드를 사용할 수 있지만, 이 메소드에서 `setState()` 를 빈번하게 사용하면, 화면을 여러번 렌더링하면서 성능 문제가 발생할 수 있으니 주의해야합니다. 왜냐하면 `componentDidMount()` 는 모든 상태값을 업데이트하고 `render()` 메소드를 호출해 렌더링한 이후에 호출되기 때문에 만약 `componentDidMount()` 에서 `setState()` 를 통해 상태값을 변경시키면 화면이 2번 이상 렌더링 될 것입니다.

그래서 리액트는 이 라이프사이클 `componentDidMount` 메소드에서 툴팁, 모달 등 화면에 렌더링 된 이후에 발생하는 이벤트인 경우에만 `setState()` 를 사용하라고 권장하고 있습니다.

```js
class Header extends React.Component {
  constructor(){
    super();
    this.state={
      hasScrolled: false,
    }
  }

  componentDidMount = () => {
    this.handleScroll();
    window.addEventListener('scroll', this.handleScroll);
  }

  handleScroll = (event) => {
    const scrollTop = window.pageYOffset;

    if (scrollTop > 20 ) {
      this.setState({ hasScrolled: true })
    } else {
      this.setState({ hasScrolled: false})
    }
  }

```

위 코드를 보세요. `window.addEventListener('scroll', this.handleScroll);` 를 통해 화면의 스크롤 이벤트를 받고 있고, `20px` 을 기준으로 이보다 많이 스크롤 했다면 상태값 `hasScrolled` 를 `true` 로 바꾸고, 그렇지 않으면 `false` 로 바꾸는 코드입니다. 만약 사용자가 스크롤을 내리면, 이 것은 화면을 한번 더 렌더링 할 것입니다.

이 것은 `componentDidMount()` 를 활용하는 방법 중 하나이고, 아래 코드와 같이 API 를 가져올 때에도 사용할 수도 있습니다.

```js
const API = " https://random/names";

class Header extends React.Component {
  constructor() {
    super();
    this.state = {
      names: "name"
    };
  }

  componentDidMount() {
    fetch(API)
      .then(response => response.json())
      .then(data => this.setState({ names: data.names }))
      .catch(() => {
        // error
      })
  }

  render() {
    const { names } = this.state;
    return (
      <div>
        <p>{names}</p>
      </div>
    );
  }
}
```

이 것은 설명을 위해 작성한 예제 코드입니다. 현재 상태에서 `const API` 는 작동하지 않고, 아무것도 반환하지 않을 것입니다. 이 코드는 초기 값으로 설정되어있는 `name` 를 먼저 화면에 렌더링 하고, 그 이후에 `fetch()` 를 통해 가져온 데이터를 바탕으로 화면에 이름이 변경될 것입니다.

## 도전 과제

1. `src/challenge/DidMount.js` 파일을 열고 `componentDidMount()` 메소드를 만드세요.

2. `componentDidMount()` 메소드내에서 `this.getData()` 메소드를 호출하여 `setTimeout()` 을 통해 1초 뒤에 상태 값이 변경되도록 만드세요.

3. `src/App.js` 에서 `{/*<DidMount />*/}` 주석을 제거하여 화면에 `DidMount` 컴포넌트가 표시되도록 만드세요.
