import React, { Component } from "react";
import DidMount from "./challenge/DidMount";
import ShouldComponentUpdate from "./challenge/shouldComponentUpdate";
import DidUpdate from "./challenge/DidUpdate";
import WillUnmount from "./challenge/WillUnmount";
import Unmount from "./example/unmount";

class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>

          <h3>Below challange codes</h3>
          {/*<DidMount />*/}
          {/*<ShouldComponentUpdate />*/}
          {/*<DidUpdate />*/}
          {/*<WillUnmount />*/}

          <h3>Below example codes</h3>
          {/*<Unmount />*/}
        </center>
      </div>
    );
  }
}

export default App;
