import React, { Component } from "react";

class WillUnmount extends Component {
  constructor(props) {
    super(props);

    this.state = {
      time: 0
    };
  }

  componentDidMount() {
    const onTick = () => {
      this.setState(state => ({
        time: state.time + 1
      }));
    };

    this.interval = setInterval(onTick, 1000);
  }

  // 1. Use componentWillUnmount() to call clearInterval(this.interval);

  render() {
    return (
      <div>
        <div>componentWillUnmount</div>
        <div>{this.state.time}</div>
      </div>
    )
  }
}

export default WillUnmount;
