// libraries
import React, { Component } from "react";

// example DIRs

import DidMount from "./example/didMount";
// import DidUpdate from "./example/didUpdate";
// import ShouldUpdate from "./example/shouldUpdate";
// import WillUnMount from "./example/willUnMount";

// challenge DIRs

// import DidMount from "./challenge/didMount";
// import DidUpdate from "./challenge/didUpdate";
// import ShouldUpdate from "./challenge/shouldUpdate";
// import DillUnmount from "./challenge/willUnMount";

class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>
          <DidMount />
          {/*<DidUpdate />*/}
          {/*<ShouldUpdate />*/}
          {/*<WillUnMount />*/}
        </center>
      </div>
    );
  }
}

export default App;
