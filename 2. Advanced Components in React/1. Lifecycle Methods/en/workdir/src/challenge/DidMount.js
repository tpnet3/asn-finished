import React, { Component } from "react";

class DidMount extends Component {
  constructor(props) {
    console.log("constructor");
    super(props);
    this.state = {
      data: "Jordan Belfort"
    };
    this.getData = this.getData.bind(this);
  }

  getData() {
    setTimeout(() => {
      this.setState({
        data: "Hello WallStreet"
      });
    }, 1000);
  }

  // 1. Use componentDidMount() to call getData();

  render() {
    console.log("render()");
    this.getData();
    return <div>{this.state.data}</div>;
  }
}

export default DidMount;
