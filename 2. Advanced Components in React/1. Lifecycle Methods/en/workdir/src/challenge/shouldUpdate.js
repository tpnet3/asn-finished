import React, { Component } from "react";

class shouldComponentUpdate extends Component {
  constructor() {
    super();
    this.state = {
      value: true,
      countOfClicks: 0
    };
    this.pickRandom = this.pickRandom.bind(this);
  }

  pickRandom() {
    this.setState({
      value: Math.random() > 0.5, // randomly picks true or false
      countOfClicks: this.state.countOfClicks + 1
    });
  }

  // use shouldComponentUpdate()
  // and use developer tool with console.log if rendering or not

  render() {
    return (
      <div>
        shouldComponentUpdate demo
        <p>
          <b>{this.state.value.toString()}</b>
        </p>
        <p>
          Count of clicks: <b>{this.state.countOfClicks}</b>
        </p>
        <button onClick={this.pickRandom}>
          Click to randomly select: true or false
        </button>
      </div>
    );
  }
}

export default shouldComponentUpdate;
