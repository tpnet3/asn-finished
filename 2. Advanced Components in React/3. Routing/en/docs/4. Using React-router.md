# Using React-router

If you have successfully finished the `Link` and `Route` components then you start to understand how you can use a component-driven development.

So far we have built two simple component which exist already in React-router. But it gave us idea how to use react-router.

Now if you open `src/example/App-1.js` file

```js
import { BrowserRouter, Route, Link } from "react-router-dom";

const App = () => (
  <BrowserRouter>


  </BrowserRouter>
)

...
```

You will see the code above

```js
import { BrowserRouter, Route, Link } from "react-router-dom";
```

here what we have built already in react-router library except `BrowserRouter` component, which is root routing component for all components. With `BrowserRouter` component you dont have to use `window.location` or `history` objects everytime but it will give you access from anywhere.

So we have done that part, you job is using `Route` and `Link` component from `react-router` and build what we have built in `App.js`.

Don't forget to write it in `App.js` as there you can run it.
