require('../db');

const BookModel = require('../db/models/books');

const result = async () => {
  await BookModel.create({
    name: "Pro React 1st ed. Edition",
    author: "Cassio de Sousa Antonio",
    pics: [
      "https://images-na.ssl-images-amazon.com/images/I/31SXwhHaDsL._SX348_BO1,204,203,200_.jpg"
    ],
    price: 27.73,
    short_desc: "some information",
    long_desc: "some information",
    date_added: new Date()
  })

  await BookModel.create({
    name: "Pro React 1st ed. Edition",
    author: "Cassio de Sousa Antonio",
    pics: [
      "https://images-na.ssl-images-amazon.com/images/I/31SXwhHaDsL._SX348_BO1,204,203,200_.jpg"
    ],
    price: 30.26,
    short_desc: "some information",
    long_desc: "some information",
    date_added: new Date()
  })

  await BookModel.create({
    name: "Pro React 1st ed. Edition",
    author: "Cassio de Sousa Antonio",
    pics: [
      "https://images-na.ssl-images-amazon.com/images/I/31SXwhHaDsL._SX348_BO1,204,203,200_.jpg"
    ],
    price: 35.56,
    short_desc: "some information",
    long_desc: "some information",
    date_added: new Date()
  })

  // 상수 `result` 에 코드를 작성해 할당하세요.
  const result

  
  console.log(result);
}

result();