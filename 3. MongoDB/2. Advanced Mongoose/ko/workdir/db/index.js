const mongoose = require('mongoose');

var dbURI = "mongodb+srv://user:1234@cluster0-xlci5.mongodb.net/test?retryWrites=true";  //for online db

mongoose.connect(dbURI, { useNewUrlParser: true })
  
mongoose.connection.on('connected', ()=>{
  console.log("Connected to Database");
});

mongoose.connection.on('error', (err)=>{
  throw new Error("Failed to Connect to Database "+err);
});

mongoose.connection.on('disconnected', (err)=>{
  throw new Error("Database Disconnected "+err);
});
