const mongoose = require('mongoose');
const BookModel = require('./models/books');

var dbURI = "mongodb+srv://user:1234@cluster0-xlci5.mongodb.net/test?retryWrites=true";  //for online db

mongoose.connect(dbURI, { useNewUrlParser: true })
  
mongoose.connection.on('connected', ()=>{
  // throw new Error("Failed to Connect to Database "+err);
});

mongoose.connection.on('error', (err)=>{
  throw new Error("Failed to Connect to Database "+err);
});

mongoose.connection.on('disconnected', (err)=>{
  throw new Error("Database Disconnected "+err);
});

const asyncFunc = async () => {
  const test = await BookModel.findOne({}).exec();
  const test2 = await test.findSamePriceBooks().exec()
  
  console.log(test2);
}

asyncFunc()