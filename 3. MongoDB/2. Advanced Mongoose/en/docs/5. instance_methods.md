# Inctance Methods

Instances of Models are documents. Documents have many of their own built-in instance methods. We may also define our own custom document instance methods too.

In the previous articles we have made Book model inside book.js. Go to books.js and replace the whole code with following code snipped: 
```js
const mongoose = require('mongoose');
// define a schema
const bookSchema = new mongoose.Schema({
  name: String,
  author: String,
  pics:[
    String
  ],
  price: Number,
  short_desc: String,
  long_desc: String,
  date_added: Number,
})

// assign a function to the "methods" object of our bookSchema
bookSchema.methods.findSamePriceBooks = function(price) {
    return this.model('Book').find({ price: this.price });
  };

module.exports = mongoose.model('Book', bookSchema);
```

Now all of our book instances have a findSamePriceBooks method available to them.
The following code demostrates how to use instance methods:
```js
  var Book = mongoose.model('Book', bookSchema);
  var book = new Book({ price: 30.00 });

  book.findSamePriceBooks(function(err, books) {
    console.log(books); // books are printed 
  });
```


In the next article we will learn about adding static methods to Models