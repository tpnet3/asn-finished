# Graphql Apollo Server Tutorial Introduction
Welcome! We’re excited that you’ve decided to learn with us.


We want you to feel confident that you have the knowledge you need to build a production-ready API with Graphql, Apollo and MongoDB, so we’re forgoing hello world in favor of a real world example complete with authentication, pagination, testing, and more. Ready? Let’s dive right in!

### Overview
For Long time REST API has dominated among developers and most of the time in order to make dynamic app, one had to apply a lot of things to make things work. Once Graphql came to existence and later Apollo(server and client), developer experience got much easier and much fun.


> I am sure you want to see how proving the technology is! Here you go, check the graphql showcase here  👉https://graphql.org/users/

In this tutorial, you’ll learn how to build Apollo GraphQL server entirely from scratch. You are going to use the following technologies:

* **GraphQL**: The rising star of backend technologies. It replaces REST as an API design paradigm and is becoming the new standard for exposing the data and functionality of a server.
* **Apollo Server**: The best way to quickly build a production-ready, self-documenting API for GraphQL clients, using data from any source.
* **GraphQL Playground**: “GraphQL IDE” that allows to interactively explore the functionality of a GraphQL API by sending queries and mutations to it. It’s somewhat similar to Postman which offers comparable functionality for REST APIs. Among other things, a GraphQL Playground…
    * ...auto-generates a comprehensive documentation for all available API operations.
    * ...provides an editor where you can write queries, mutations & subscriptions, with auto-completion(!) and syntax highlighting.
    * ...lets you easily share your API operations.
* **MongoDB with Moongoose**: MongoDB is the most famous No-SQL databse technology built to scale. Mongoose is an elegant mongodb object modeling for node.js. During this tutorial, I am sure you will find learning MongDB and mongoose the easiest thing in the world 💯

![apollo graphql](https://www.apollographql.com/docs/apollo-server/images/index-diagram.svg)

### Goal of this tutorial

The goal of this tutorial is to make sure you understand intermediate level of building Graphql Apollo Server API and to build a real API for BounceCode Bookstore test web app in Project Building phase of this MERN Course. 
> You can see the ready final BounceCode Bookstore test web app server side playground here 👉 https://mern-course-server.herokuapp.com/graphql

Have you opened the BounceCode Bookstore server API playground? Before moving to the next section, I think you would love to check if the server API works or not! The screen is devided into 2 parts. On the left side write the following query: 
```graphql
# Write your query or mutation here
query {
  getItem(_id: "5c5bc694c17cf2125fa0438a"){
    name
    author
    pics
    price
    short_desc
    long_desc
    date_added
  }
}
```
Press the Play button at the center. You should be able to see the output on the right side of the screen.
Is not it great to access server API like this? 

Now, it is time to get our hands dirty by making the entire API through following this tutorial 🚀