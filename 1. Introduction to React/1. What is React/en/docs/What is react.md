# What is React.js

> React.js is a javaScript **`view`** library developed by Facebook engineers.

## Why React

In recent years, React.js has become very popular javascript library as it brought brand-new concepts and tools to the front-end development and we will discover them one by one during our course.

## Background and pre-requisites

In order to follow this course you need to have basic understanding of **HTML, CSS, JavaScript and ES6**

## Test the Editor

Open `Files/src/App.js`, you should see these codes.

```js
import React, { Component } from 'react';

class App extends Component {
  render() {
      
    return (
      <div>
        <center>
         <h1>hello world</h1>
        </center>
      </div>
    );
  }
}

export default App;
```
And click **`Run`** button.

If you see **`hello world`** on our embeded browser then you good to go.



