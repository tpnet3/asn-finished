# React.js 가 뭔가요?

> React.js 는 페이스북 개발자에 의해 개발된 자바스크립트 **`뷰`** 라이브러리입니다.

## 왜 React 를 사용할까요?

최근, React.js 는 매우 인기있는 자바스크립트 라이브러리이고, 프론트엔드 개발에 새로운 개념을 가져왔습니다. 이를 통해 더 화려한 웹 어플리케이션 서비스를 만들 수 있습니다. 이 코스에서 그 내용들을 하나씩 알아보겠습니다.

## 전제 조건

이 코스를 이해하기 위해서는 **HTML, CSS, JavaScript 그리고 ES6** 에 대한 기초를 이해하고 있어야합니다.

## 첫번째 프로그램 실행하기

`src/App.js` 파일을 열면, 다음과 같은 코드가 보일 것입니다.

```js
import React, { Component } from 'react';

class App extends Component {
  render() {
      
    return (
      <div>
        <center>
         <h1>hello world</h1>
        </center>
      </div>
    );
  }
}

export default App;
```

## 도전 과제

**`Run`** 버튼을 클릭하여 임베디드 브라우저에서 **`hello world`** 가 표시되고 잘 작동하고 있는지 확인해보세요.

잘 작동하는 것을 확인했다면, 다음 단계로 진행하세요.