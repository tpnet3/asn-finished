# States - Part 2  (Using setState)

> 지난 섹션에서 우리는 `States` 와 `Props` 에 대해 알아봤습니다. 이번 섹션에서는 `States` 에 대해 좀 더 자세히 알아보려고 합니다. `States` 는 `Props` 와 비슷하지만, 이 데이터를 처리하는 방식은 Props 와 약간 다릅니다.

우리는 지금까지 아래 코드와 같은 States 가 없는 함수형 컴포넌트를 만들어봤습니다. 하지만 이것은 상태값 없는 컴포넌트 (stateless components) 라고 불리기도 합니다.

```js
const AnotherHeader = (props) => (
  <h1> Hello, {props.name}</h1>
)
```

### 언제 States 를 사용 해야 할까요?

States 는 랜더링할 때 사용되는 변수를 저장하기 위해 사용됩니다. 예를 들어, 전등은 전등을 켰을 때 `on` 상태를 유지하고, 전등을 껐을 때 `off` 상태로 바뀌어야합니다.

그리고 상태를 바꾼 후에 화면에 그 결과를 다시 랜더링해줘야합니다. 그래서 `States` 의 데이터를 업데이트하기 위해서는, `this.setState()` 를 사용합니다.

아래와 같은 예제 코드를 보세요.

```js
class Switch extends React.Component {

  // 상태의 초기 값을 할당했습니다.
  state = {
    isLightOn: true
  };

  handleClick = () => {
    // prevState 는 현재 상태의 state 값입니다.
    this.setState(prevState => ({

      // prevState.isLightOn 값이 false 일 경우 true, true 일 경우 false 로 설정합니다. 
      isLightOn: !prevState.isLightOn,
    }))
  }

  render () {
    return( 
      <button
        {/* 버튼을 클릭했을 때 handleClick 함수를 실행합니다. */}
        onClick={this.handleClick}
      >
        {/* JSX 에서 조건문을 사용하기 위해서는 3항 연산자를 사용해야합니다. */}
        {this.state.isLightOn ? 'ON' : 'OFF'}
      </button>
    )
  }
}
```

`Switch` 클래스는 `state` 객체를 가지고 있고, 이 것은 리액트에서 사전에 약속된 객체입니다. 그리고 이 객체 안에 `isLightOn` 키를 추가했고, 키의 기본 값은 `true` 입니다.

이제, 버튼을 클릭하면 `this.handleClick()` 함수가 실행되고, 함수 내에서 `this.setState()` 가 실행됩니다.

`this.setState()` 의 인자는 함수나 객체, 두가지 방법으로 전달할 수 있습니다. 만약 함수를 인자로 전달할 경우, 전달한 함수의 첫번째 인자는 이전 사태 값이고, 반환되는 값이 설정할 state 객체입니다. 반환되는 값은 반드시 객체여야합니다.

앞에서 설명했듯이, `this.setState()` 에는 객체를 전달해도 됩니다. 아래 코드는 위 예제와 비슷하게 작동하는 코드입니다.

```js
handleClick = () => {

  // this.setState() 의 인자로 함수를 전달하지 않고 변경할 객체로 전달했습니다.
  this.setState({
    isLightOn: !this.state.isLightOn
  })
}
```

## State 가 이전 값에 영향을 받을 때에는 함수로 전달하는 것이 안전할 수 있습니다.

우리는 앞에서 배운 State 를 활용해 아래와 같은 간단한 카운터 앱을 만들 수 있습니다.

하지만 상태값을 변화시킬 때 이전 값의 영향을 받고 동시에 여러번 상태값 업데이트를 수행하는 경우에는 함수를 사용하는 것이 더 안전할 수 있습니다.

예를 들어, 아래와 같은 코드가 있다고 생각해보세요.

```js
class Counter extends React.Component {
  state = {
    count: 0
  }

  incrementCount = () => {

    // 상태값 업데이트를 3번 반복합니다.
    for (let i = 0; i < 3; i++) {
      this.setState({
        count: this.state.count + 1
      })
    }
  }

  render() {
    return (
      <button onClick={this.incrementCount}>
        <div>{this.state.count}</div>
      </button>
    )
  }
}
```

위 코드의 `this.state.count` 값은 몇 씩 증가할까요? `this.setState()` 는 비동기로 작업이 이루어지고, 위 코드는 아래와 같은 명령을 3번 반복한 것과 같습니다.

```js
this.setState({
  // this.state.count 값이 0 인 시점에서 3번 반복합니다.
  count: 0 + 1
})
```

그렇습니다, `this.state.count` 는 `1` 씩 증가합니다. 하지만 인자로 함수를 전달할 경우, 결과 값은 달라집니다. 아래 코드를 보세요.

```js
// 상태값 업데이트를 3번 반복합니다.
for (let i = 0; i < 3; i++) {
  this.setState((prevState) => {
    count: prevState.count + 1
  })
}
```

`this.setState()` 의 인자로 함수를 전달할 경우, 비동기 상태에서 함수를 실행하게 되며, 이전 값을 누적하여 기록하게 됩니다. 이 코드의 결과 값은 `3` 씩 증가합니다.

따라서 이전 값을 사용하여 상태 값을 업데이트 하고자 할 때에는 함수를 사용하여 업데이트하는 것이 더 좋을 수 있습니다.

## 도전 과제

여기, 카운터 컴포넌트가 있습니다.

```js
class Counter extends React.Component {
  state = {
    count: 0
  }

  resetCount = () => {
    // TODO: 상태 값이 초기화 되도록 만드세요.
  }

  incrementCount = () => {
    for (let i = 0; i < 3; i++) {
      // TODO: 상태 값이 이전 상태값의 영향을 받아 증가하도록 수정하세요.
      this.setState({
        count: this.state.count + 1
      })
    }
  }

  render() {
    return (
      <div>
        {/* TODO: RESET 버튼이 반응하도록 만드세요. */}
        <button>RESET</button>
        <button onClick={this.incrementCount}>INCREMENT</button>
        <div>{this.state.count}</div>
      </div>
    )
  }
}
```

1. RESET 버튼을 클릭 했을 때 this.resetCount() 를 호출하도록 만드세요.

2. 리셋 버튼을 눌렀을 때 상태 값이 0으로 초기화 되도록 만드세요.

2. this.setState() 의 인자로 함수를 전달하여 클릭했을 때 3씩 증가하도록 수정하세요.