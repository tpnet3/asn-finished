# 모든 것은 컴포넌트(Component) 입니다.

> 리액트에서, 웹사이트에서 볼 수 있는 모든 UI 엘리먼트들은 컴포넌트입니다.

## 컴포넌트가 무엇이고 왜 필요한가요?

컴포넌트는 재사용하기 쉽고 단순합니다. 매번 버튼, 폼, 섹션  처럼 똑같은 엘리먼트들을 두번 이상 만들 필요가 없습니다. 한번 만들고 모든 곳에서 재사용할 수 있습니다. 이 것은 개발을 훨씬 빠르게 할 수 있고, 더 멋진 앱을 만들어낼 수 있습니다.

## 컴포넌트를 어떻게 만들 수 있나요?

에디터에서 App.js 파일을 여세요.

```bash
├─ project
│  └─ src
│     ├─ App.js
│     └─ index.js
```

이것은 코드를 이해하기 위한 우리의 첫번째 컴포넌트입니다.

```js
import React, { Component } from 'react';

// 클래스 App 은 React.Component 로부터 확장된 클래스입니다.
class App extends Component {

  // React.Component 로부터 확장된 클래스는 render() 라는 메소드를 가지고 있습니다.
  render() {

    // 리턴된 값은 리액트팀에 의해 만들어진 JSX 값입니다. HTML 을 사용하는 것과 비슷합니다.
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>
        </center>
      </div>
    );
  }
}

export default App;
```

추측할 수 있듯이 이 것은 `React.Component` 로부터 확장된 ES6 클래스입니다.

우리의 `App` 클래스는 `render()` 라는 하나의 메소드를 가지고 있습니다. 이 메소드는 리액트 컴포넌트에서만 꼭 필요합니다. 이 메소드에서 반환된 값은 웹페이지에서 어떻게 랜더링 할지 결정합니다.

> 만약 ES6 클래스에 대한 추가 내용이 필요하다면 [ES6 classes by Mozilla](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)
를 보거나 `ES6 for React` 코스를 다시 학습할 수 있습니다.

`return` 값에 대한 문법은 일반적인 자바스크립트 문법처럼 보이지 않습니다. 왜냐하면 이 것은 리액트 팀에 의해 만들어진 JSX (JavaScript extension syntax) 이기 때문입니다.

JSX는 HTML과 비슷한 문법을 가지고 있습니다. 이 값은 나중에 일반적인 자바스크립트로 컴파일됩니다. JSX가 무엇인지 이해하지 못했더라도 걱정할 필요가 없습니다. 다음 문서에서 JSX 문법에 대해 더 자세히 배웁니다.

## 도전 과제

> 다음 섹션으로 넘어가기 전에 도전과제를 수행하세요.

`<center></center>` 태그 안에 새로운 `<p></p>` 태그를 만들어서  `Hello, React` 라는 글자를 입력한 후 **Run** 버튼을 눌러보세요. 이미 실행된 상태라면 변경된 내용이 자동으로 적용될 것입니다.
