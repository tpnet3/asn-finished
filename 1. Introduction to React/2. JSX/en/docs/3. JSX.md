# JSX

React builds our apps with a fake representation of the **`Document Object Model`** **`(DOM)`**. And React calls this the _`virtual DOM`_. We do not get into deep details for now but to grasp the idea we will see some examples.

> DOM refers to HTML tree which makes up a web page.

```js
   <!doctype html>
  <html lang="en">
  <head></head>
  <body>
      <ul class="list">
        <li class="list__item">List item</li>
      </ul>
  </body>
  </html>
```

Above code refers to this DOM tree

```bash
html
  ├──head
  ├──body
      ├──ul
          ├──li
              ├──"List item"
```

In order to create another **`list`** item we can do

```js
<!doctype html>
  <html lang="en">
  <head></head>
  <body>
      <ul class="list">
        <li class="list__item">List item</li>
      </ul>
  </body>

  <script>
    var listItemOne = document.getElementsByClassName("list__item")[0];
listItemOne.textContent = "List item one";

var list = document.getElementsByClassName("list")[0];
var listItemTwo = document.createElement("li");
listItemTwo.classList.add("list__item");
listItemTwo.textContent = "List item two";
list.appendChild(listItemTwo);
  </script>
  </html>
```

Above code inside **`<script> </script>`** tag is called **DOM API**, it helps us to _`create, update, delete`_ HTML tree. Now our tree looks like this.

```bash
html
  ├──head
  ├──body
      ├──ul
          ├──li
              ├──"List item one"
          ├──li
              ├──"List item two"
```

We created second _`list item`_ with the help of **DOM API**

> If you want to see for yourself the DOM tree view, you can paste above code to this website.

[Live DOM Viewer](http://software.hixie.ch/utilities/js/live-dom-viewer/#)

Updating, creating, deleting DOM tree with **`React`** would be

```js
import React from "react";
import ReactDOM from "react-dom";

const list = React.createElement(
  "ul",
  { className: "list" },
  React.createElement("li", { className: "list__item" }, "List item")
);

ReactDOM.render(list, document.body);
```

Same idea here but **React** uses **`virtual DOM`**. The difference is _`old DOM`_ updates the whole HTML tree but **`virtual DOM`** updates DOM tree where needed to be updated. That's the job of **`React.DOM`**. And in order to make the syntax easier and cleaner, React Team created JSX. JSX works just like DOM API but with HTML like syntax.

Our code looks like this in **`JSX`**

```js
<ul className="list">
  <li className="list__item">List item one</li>
  <li className="list__item">List item two</li>
</ul>
```

We will cover little more about **`JSX`** and its difference with **`HTML`** in our next _JSX part 2_ section.

But before going please try to complete this challenge.

---

```bash
├── project
│   ├── src
│       ├── App.js
│       ├── index.js
```

Create new **`Components`** folder in your **`src`** folder. Inside this folder create new **`MyComponent.js`** file. And copy all codes from **`App.js`**, paste them in **`MyComponents.js`** and change `class App extends Component` into `class MyComponent extends Component`, don't forget to change the `export default App` into `export default MyComponent`. Copy all the code below and paste them inside **`App.js`**.

```js
import React, { Component } from "react";
import MyComponent from "./Components/MyComponent";

class App extends Component {
  render() {
    return <MyComponent />;
  }
}

export default App;
```

If you have done all steps correctly you should see
