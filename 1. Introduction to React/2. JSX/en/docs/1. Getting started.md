# Folder structure

At the end of first section, you have run **`hello world`** and most likely you have seen this folder structure.

```bash
├── project
│   ├── .cache
│   ├── dist
│   ├── node_modules
│   ├── public
│   ├── src
│    ├── .babelrc
│    ├── .gitignore
│    ├── package-lock.json
│    ├── package.json
│    ├── README.md
```

If you are overwhelmed by this, do not worry, We are using [Create React App](https://facebook.github.io/create-react-app/) tool by facebook which configures all the set up tools like babel and webpack to up and running single page React apps. So most of these folders are set up folders of create-react-app, you should not delete them,they are important to run our app. We will introduce them a bit but if you feel like you need to know more you can check [create-react-app documentation](https://facebook.github.io/create-react-app/docs/folder-structure).
All **`npm modules`** are saved automatically in **node_modules** folder, **public** and **dist** folders are only for tools like _babel_ and _webpack_, you should have heard them in our ES6 section. We only use **src/** folder for our development purpose.

If you have no problem with understanding above mentioned stuffs then we good to go to the next section.
