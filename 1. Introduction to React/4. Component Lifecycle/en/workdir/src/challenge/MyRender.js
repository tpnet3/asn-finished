import React, { Component } from "react";

class MyRender extends Component {
  constructor() {
    super();
    // 1. create counter state
  }

  // 2. create incrementCount() method which increments the component state by 1
  // optional in setState use callback not object.

  render() {
    return (
      <button onClick={this.incrementCount}>
        <div>{this.state.counter}</div>
      </button>
    );
  }
}

export default MyRender;
