# Adding Context to Apollo Server

### What is Context in Apollo Server
This is an object shared by all resolvers in a particular query, and is used to contain per-request state, including authentication information, dataloader instances, and anything else that should be taken into account when resolving the query.
### Context argument
The **context** is the third argument passed to every resolver. It is useful for passing things that any resolver may need, like **authentication scope**, database connections, and custom fetch functions. Additionally, if you’re using **dataloaders to batch requests** across resolvers, you can attach them to the **context** as well.<br>
As a best practice, **context** should be the same for all resolvers, no matter the particular query or mutation, and resolvers should never modify it. This ensures consistency across resolvers, and helps increase development velocity.<br>
Creating **context** is done in index.js file where we created our ApolloServer.
```js
const context = ({ req }) => {
  const accessToken = req.headers.authorization || null; 
  return {
    req,
    headers: {
      accessToken: accessToken
    }
  }
};
```
If you noticed from the code snippet above, from client request we are fetching **accessToken**. When you learn about connecting client to server API you will learn how to send **accessToken** with request to API. <br>
To provide a **context** to your resolvers, add a **context** object to the Apollo Server constructor. This constructor gets called with every request, so you can set the context based off the details of the request (like HTTP headers).
```js
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context,
});
```

That is it😇😇😇. We have added **context** object which can be accessed in any resolver. In the next section we shall re-implement **likeBook** method in accordance with context. 