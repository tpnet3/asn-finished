## Removing elements

> In this article we will learn how to remove element in JavaScript

## `remove()` 

Deleting an element is easy. All DOM nodes carry `remove()` function as built in. Look at the code below:

```js
// From html DOM-Tree bring element element with the id of root
const rootElement = document.getElementById("root");

// delete rootElement
rootElement.remove();
```

From now on the element we deleted using `remove()` function can not be seen on screen.

## Article Challenge

Try to delete the element with id of `root`.