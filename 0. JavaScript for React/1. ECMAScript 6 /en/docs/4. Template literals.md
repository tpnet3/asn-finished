# Template Literals

- This new feature of ES6 improves your JavaScript code readability when working with Strings.

And **`Template literal`** is a way to concatenate strings while allowing embedded expressions and improving readability.

### Strings

In JavaScript normally **`single`** (') or **`double`** (") **`quotes`** were used **_(still being used)_**.

```js
const str1 = "hello world";
const str2 = "hello world";
```

Using **`Template literals`**

```js
const str = `hello world`;
```

Yep, **`backticks`**, output will be same and this way initializing also give us `string`.

Now lets see them in different use cases to get know the advantages of **`template literals`** over old way of initializing strings with **`quotes`**.

### String Concatenation

```js
const firstName = "John";
const lastName = "Larson";

console.log(
  "Hello, My name is" + firstName + ", " + "my last name is" + lastName + "."
);
```

The problem with above example code is its confusing, you need to think about spacings(dots, commas) and everytime, when you want to insert **`variables`**, you use **`+`**. I think, its confusing.

Now using **`Template literals`**

```js
const firstName = "John";
const lastName = "Larson";

console.log(`Hello, my name is ${firstName}, my last name is ${lastName}`);
```

You might think **`+`** was easier than **`$`** and **`{}`**, maybe. But look at the syntax, whatever you write, output will be the same.

### Multiple line

When we need use multi line **`strings`** we need to **_hack_** with **`quotes`**.

```js
console.log("Hello, \n"
"my first name is John, \n"
"my last name is Larson.");

```

**`\n`** gives us **`new line`** and in every line we need to open **`quotes`** and close it.

```js
console.log(`Hello,
my first name is John,
my last name is Larson.
`);
```

With **`backticks`** its easy as it outputs whatever you write.

---

Challenge!

Change all into ES6 **`template literals`**

1.

```js
const name = "Brendan";
console.log("Hello, " + name + " !.");
```

2.

```js
console.log("Two plus two is " + (2 + 2) + ".");
```

3.

```js
console.log("Dear Mr.Smith,\n" + "Hope you are well.\n" + "\t Sincerely, John");
```
