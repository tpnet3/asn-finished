const mongoose = require('mongoose');
// define a schema
const bookSchema = new mongoose.Schema({
  name: String,
  author: String,
  pics:[
    String
  ],
  price: Number,
  short_desc: String,
  long_desc: String,
  date_added: Number,
})

// assign a function to the "methods" object of our bookSchema
bookSchema.methods.findSamePriceBooks = function(price) {
  return this.model('Book').find({ price: this.price });
};

bookSchema.statics.findByName = function(name, cb) {
  return this.find({ name: new RegExp(name, 'i') }, cb);
};
module.exports = mongoose.model('Book', bookSchema);