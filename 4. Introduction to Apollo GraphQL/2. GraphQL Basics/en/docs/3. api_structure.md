# Understanding API Structure

In the previous chapter we have learned the Apollo server and played around with running it. When we presented the this code: 

```js
const { ApolloServer, gql } = require('apollo-server-express');
const express = require('express')
const book = [
  {
    name: 'Harry Potter and the Chamber of Secrets 2',
    author: 'J.K. Rowling',
    pics:[
        "https://is3-ssl.mzstatic.com/image/thumb/Video118/v4/a5/c0/fb/a5c0fbaa-2fb5-fefa-0e2e-552207b0376f/pr_source.lsr/268x0w.png"
    ],
    price: 20,
    short_desc: "This is interesting book",
    long_desc: "The book was published in the United Kingdom on 2 July 1998 by Bloomsbury and later, in the United States on 2 June 1999 by Scholastic Inc. Although Rowling says she found it difficult to finish the book, it won high praise and awards from critics, young readers and the book industry, although some critics thought the story was perhaps too frightening for younger children.",
    date_added: 1550109654908,
  },
  {
    name: 'Jurassic Park',
    author: 'Michael Crichton',
    pics:[
        "https://upload.wikimedia.org/wikipedia/en/thumb/3/33/Jurassicpark.jpg/220px-Jurassicpark.jpg"
    ],
    price: 10,
    short_desc: "This is interesting book",
    long_desc: "Jurassic Park is a 1990 science fiction novel written by Michael Crichton, divided into seven sections (iterations). A cautionary tale about genetic engineering, it presents the collapse of an amusement park showcasing genetically recreated dinosaurs to illustrate the mathematical concept of chaos theory and its real world implications.",
    date_added: 1550109654910,
  },
];
const typeDefs = gql`
  type Book {
    name: String
    author: String
    pics:[
       String
    ],
    price: Float,
    short_desc: String,
    long_desc: String,
    date_added: Float,
  }
  type Query {
    books: [Book]
  }
`;
const resolvers = {
  Query: {
    books: () => book,
  },
};
const server = new ApolloServer({ typeDefs, resolvers });
const app = express();
server.applyMiddleware({ app });
const port = process.env.PORT || 4001;
app.listen({ port }, () =>
  console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`),)
```
you must have had some confusion about what is going on in the code snippet. 
As we said before in order to make ApolloGraphql Server API we need **ApolloServer**, **Graphql Schema** and **Resolvers**. If you notice, following line of code is creating **ApolloServer** class instance with two essential arguments(props), which are **typeDefs** and **resolvers** and creates Server which can handle requests according to those two props. 

```js
const server = new ApolloServer({ typeDefs, resolvers });
```

Let's explore those two props. 

* **typeDefs**: this indicates Graphql Schema which is created as like JSON objects and translated into graphql objects using gql:

```js
const typeDefs = gql`
  type Book {
    name: String
    author: String
    pics:[
       String
    ],
    price: Float,
    short_desc: String,
    long_desc: String,
    date_added: Float,
  }
  type Query {
    books: [Book]
  }
`;
```
* **resolvers**: resolvers are driving engines of queries. Look at the snippet and you will notice that resolvers are actually functions: 

```js
const resolvers = {
  Query: {
    books: () => books,
  },
};
```

### Article Challenge

*We said that resolvers are actually functions, and if you noticed in the code snippet above those resolver functions are written as arrow functions(ES6). Try to change them into traditional JS function.*

In the next article we split this code into folders for better housekeeping of our project directory. 

