const books = require('./query/books')
module.exports = {
    Query: {
        books: books.books,
        getSingleBook: books.getSingleBook
    }
}