const Book = require('./../../db/models/books.js');

const books = async ()=>{
  const result = await Book.getBooks();
  if(result===null){
  return null;
  }
  return result;
}
  
  module.exports = {
      books
  }