# Query Aliases
Since the result object fields match the name of the field in the query but don't include arguments, you can't directly query for the same field with different arguments. That's why you need aliases - they let you rename the result of a field to anything you want.

Imagine you want fetch books with two different price arguments in getSingleBook query, then you need aliases: 
* query:
```graphql 
query{
    cheaperBook: getSingleBook(price: 5.0){
        name
    }
    expensiveBook: getSingleBook(price: 50.0){
        name
    }
}
```
* result: 
```graphql
{
  "data": {
    "cheaperBook": {
      "name": "Cheap Book Name"
    },
    "expensiveBook": {
      "name": "Expensive Book Name"
    }
  }
}
```
In the above example, the two getSingleBook fields would have conflicted, but since we can alias them to different names, we can get both results in one request.

### Article Challenge
*Try to call books query with aliases in Playground*

In the next article we shall learn about naming operations while querying.

