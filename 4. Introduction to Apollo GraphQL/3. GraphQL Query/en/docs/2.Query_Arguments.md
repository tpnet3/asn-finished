# Query Arguments 

If the only thing we could do was traverse objects and their fields, GraphQL would already be a very useful language for data fetching. But when you add the ability to pass arguments to fields, things get much more interesting.

In order to test arguments of graphql in real life example, we should build a query where we can pass argument(condition) to fetch data from API according to the arguments. 
For this purpuse let's make getSingleBook query with price argument.<br> 
![data flow](https://res.cloudinary.com/ddjg5k4ul/image/upload/v1552964319/Frame_2_1.png)
Data Flow in ApolloGraphql API Architecture.<br>
According to the data flow in our architecture, it is better to start building queries from Database Model where we fetch real data. Following steps will explain how to make query with arguments in accordance with our architecture:
* In the file where we have defined Book Mongoose Model we should add a static function where we fetch a book with given price argument: 
```js
bookSchema.statics.getSingleBook = async function (price) {
    const result = await this.findOne({price: price}).exec(); //this will return a book with given argument
    if(!result){
        return null
    }
    else{
        return result
    }
}
```
* Next, define getSingleBook resolver function inside resolvers/query/books.js and export this function: 
```js
const getSingleBook = async (obj, {price})=>{
    const result = await await Book.getSingleBook(price);
    if(result===null){
    return null;
    }
    return result;
}
module.exports = {
    books, //already exported before
    getSingleBook //newly created method
}
```
* Following that, lets point new getSingleBook in resolvers entry point file, namely index.js resolvers folder. Change the whole code into this: 

```js
const books = require('./query/books')
module.exports = {
    Query: {
        books: books.books,
        getSingleBook: books.getSingleBook
    }
}
```

* Last, we should add new query to index.js inside schema folder: 

```js
const { gql } = require('apollo-server');
const types = require('./types');
const query = gql`
  type Query {
      books: [Book],
      getSingleBook(
          price: Float
      ): Book
    }
  `
  module.exports = [query, types];
  ```

Defining a query with argument is finished, now you can check this query in Playground.<br>
To query with argument you can write this in Playground: 
```graphql
query{
    getSingleBook(price: 12.09){
        name
        author
    }
}
```
> If there is a book given the condition(price = 12.09), the response should be a book, otherwise null.

In the next article we shall explore query aliases. 

