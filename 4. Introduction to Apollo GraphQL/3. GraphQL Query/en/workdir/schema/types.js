const {gql} = require('apollo-server-express')

module.exports = gql`
type Book {
name: String
author: String
pics:[
   String
],
price: Float,
short_desc: String,
long_desc: String,
date_added: Float,
}
`