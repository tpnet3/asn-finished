import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'


import Index from './screens/index'
import Signup from './screens/signup'


class App extends Component{
  render(){
    return(
      <main>
        <Switch>
          <Route exact path='/' component={Index}/>
          <Route exact path='/signup' component={Signup}/>
        </Switch>
      </main>
    )
  }
}

export default App;
