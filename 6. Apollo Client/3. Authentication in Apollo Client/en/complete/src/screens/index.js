import React, { Component } from 'react';
import '../App.css';


import { Query } from "react-apollo";
import gql from "graphql-tag";

const GET_ITEM = gql`
query getItem($_id:String!){
  getItem(_id: $_id){
    name
    author
    price
    pics
    short_desc
    long_desc
    date_added
  }
}
`

class App extends Component {
  constructor(){
    super()
    this.state={
      ME: 'DEEN'
    }
  }
  render() {
    return (
      <Query query={GET_ITEM} variables={{_id: '5c544c28149c161e5dfff12c'}}>
      {({loading, data, error})=>{
        if(loading)return<p>Loading ...</p>
        if(error)return<p>{error}</p>
        //if both error and loading are false this means data has some returned value
        var jsDate = new Date(data.getItem.date_added);
        return(
          <div className="App">
            <header className="App-header">
              {data.getItem.pics.map((pic)=>{
                return <img key={pic} src={pic} className="image" alt="book_pic" />
              })}
              <p className='title'>
                {data.getItem.name}
              </p>
              <p>by <b>{data.getItem.author}</b></p>
              <p>Price <b>{data.getItem.price}$</b></p>
              <p>Description:</p>
              <blockquote>
                {data.getItem.short_desc}
              </blockquote>
              <blockquote>
                {data.getItem.long_desc}
              </blockquote>
              <p>Date Added: <b>{jsDate.toUTCString()}</b></p>
              <input  type='text' onChange={e=>{this.temp(e.target.value)}}/>
            </header>
          </div>
        )
      }}
      </Query>
    );
  }
}

export default App;
