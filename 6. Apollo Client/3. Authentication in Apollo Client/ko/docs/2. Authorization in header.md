# Authorization using Header and localStorage
First of all, let's do some housekeeping!
### Housekeeping 
As we have explored before we define all the `clinet` connection and `ApolloProvider` in the starting point of the application, either in index.js or App.js. In the previous turorials we created our `ApolloClient` and wrapped our `App` in `index.js` file. So, now to the same file and we will start real fun!

### Clear things up
Initially, as we mentioned in the previous article we are not using `apollo-boost` since `apollo-boost` doesn't allow the `HttpLink` instance it uses to be modified, we have to modify the index.js file a lot. Therefore now delete everything and paste this code snippet there: 
```js
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom'

ReactDOM.render(
    <BrowserRouter>
      <App />
    </BrowserRouter>
  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
```
As you can see, we are strating it from fresh. 

Now follow upcoming steps to achieve understanding of fully working `user authentication` enabled `ApolloClient`. 
* Install required new dependencies: 
```
npm i apollo-link-http apollo-link-context apollo-cache-inmemory
```
* Import required modules: 
```js
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from "react-apollo";
```
`createHttpLink` is used establish `http link` over client and server. The http link is a terminating link that fetches GraphQL results from a GraphQL endpoint over an http connection. The http link supports both POST and GET requests with the ability to change the http options on a per query basis.
The `setContext` function takes a function that returns either an object or a promise that returns an object to set the new context of a request.
`InMemoryCache` is a normalized data store that supports all of Apollo Client features without the dependency on Redux.

* Next,using `createHttpLink` function define graphql endpoint link:
```js
const httpLink = createHttpLink({
  uri: 'https://mern-course-server.herokuapp.com/graphql',
});
```
* Construct new context of the request of our `ApolloClient`. This part somewhat important to understand. As we mentioned earlier, predefined `setContext` function takes a function that returns either an object or a promise that returns an object to set the new context of a request.
```js
const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('accessToken');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : "",
    }
  }
});
```
Above, inside function that we are passing inside `setContext` is getting token from `localStorage`, if you can remember from the previous section, in a article challange of Mutation implementation you set `accessToken` in localStorage. Here, we are defining the function that gets token if exists. If it exists and token is set, then that means user has signed up. Does not it? Inside headers we are defining new field inside `headers`, namely `authorization`. If you can remember from the server side authorization implementation we have fetched `authorization` field from every requests' header.

* Following this, create client instance: 
```js
const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});
```
* Last, just wrap your app with `ApolloProvider`: 
```js
ReactDOM.render(
    <ApolloProvider client={client}>
        <BrowserRouter>
        <App />
        </BrowserRouter>
    </ApolloProvider>
  , document.getElementById('root'));
```

That is it. Authentication is done. Now in our App when user commits any `mutation` or `query` that needs user authentication, user first have to sign up/or log in then he/she can do that action. All the underlying programming is carried out by ApolloClient authentication scheme we created.

### Article Challenge
As you know, in server side implementation of Apollo we have made `likeBook` mutation. And, this mutation needs authorization of the user. In this challenge, please try to implement this `likeBook` in a separate screen. 
  * Make a new screen in your SPA(Single Page application) 
  * Implement `likeBook` in the new screen. [Here](https://mern-course-server.herokuapp.com/graphql), you can find and learn that mutation's nature. Note that in the playground `likeBook` is implemented as `likeItem`.


