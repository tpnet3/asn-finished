# Writing Data to Graphql Endpoint With Mutation
The `Mutation`component is what you’ll use to trigger mutations from your UI. To create a `Mutation` component, just pass a `GraphQL mutation string` wrapped with the `gql` function to `this.props.mutation` and provide a function to `this.props.children` that tells React what to render. The Mutation component is an example of a React component that uses the `render prop` pattern. React will call the render prop function you provide with a mutate function and an object with your mutation result containing loading, error, called, and data properties. 

### Housekeeping
In order to bring Mutation into our React App we need to make a new page. As you know, in order to make an SPA(Single Page Application) in React we need react-router. We assume that you are able to make SPA out of our React App we have built so far with Apollo Client. Therefore we do not cover making SPA in this chapter.
To test Mutation we will call to `signUpWithPassword` mutation that is already available in our BookStore test Graphql Endpoint. [Here](https://mern-course-server.herokuapp.com/graphql) you can check the `signUpWithPassword` nature.

### Mutation in Action
We assume that you have created a new page called Signup.js. Inside that file, we will create a page where user can sign up to our application. User has to be able to write his username and password. Therefore, you should make two inputs, one for username which is `text` type and the second one is for the password which is `password` type. 
So it simply can look like this: 
```js
import React, { Component } from 'react'

export default class Signup extends Component {
  render() {
    console.log("something")
    return (
      <div>
        <form>
            <label>Username
                <input type='text' name='username' placeholder='username'/>
            </label>
            <label>Password
                <input type='password' name='password' placeholder='password'/>
            </label>
            <button type='submit'>SignUp</button>
        </form>
      </div>
    )
  }
}
```
Now, we are ready to implement `Mutation` into our singup page. In order to complete it follow these steps:
* Import necessary modules. For mutation we need `Mutation` and `gql`. So in singup.js file inmport those modules.
```js
import {Mutation} from 'react-apollo'
import gql from 'graphql-tag'
```
* Next, we should build mutation object using gql. But before that let's explore how `signUpWithPassword` mutation works:
![image](https://res.cloudinary.com/ddjg5k4ul/image/upload/v1554356941/Screen_Shot_2019-04-04_at_2.48.20_PM.png)
If you can see from the image above, `signUpWithPassword` is a mutation and it takes two arguments, those are `username` and `password`. Both arguments are `String`. Next things worth mentioning is that the mutation only returns `unnamed` `String`. This string is actually an access token which we made in the serverside graphql tutorial. Because the mutation returns only one unnamed `String` we do not need to mention it in our mutation object that we will make next using `gql`:
```js
const SIGN_UP = gql`
mutation signUpWithPassword($username: String!, $password: String!){
  signUpWithPassword(username: $username, password: $password)
}
`
```
* Following this, we can implement `Mutation` into React main Component of singup page. First Let's see the full code: 
```js
export default class Signup extends Component {
    constructor(){
        super();
        this.state={
            username: '',
            password: ''
        }
    }
  render() {
    return (
    <Mutation mutation = {SIGN_UP} onCompleted={()=>{alert('Signup complete!')}} onError={()=>{alert('Error happened!')}}>
      {(signUpWithPassword)=>{
          return(
            <div>
                <form>
                    <label>Username
                        <input type='text' name='username' placeholder='username' onChange={(e)=>{this.setState({username: e.target.value})}}/>
                    </label>
                    <label>Password
                        <input type='password' name='password' placeholder='password' onChange={(e)=>{this.setState({password: e.target.value})}}/>
                    </label>
                    <button type='submit' onClick={(e)=>{
                        e.preventDefault()
                        signUpWithPassword({variables:{username: this.state.username, password: this.state.password}})
                    }}>SignUp</button>
                </form>
            </div>
          )   
      }}
    </Mutation>
    )
  }
}
```
Again you might have a lot how questions that start with "Why?" or "How?". So, Let's divide the code above and learn conquer it:
* We first made two state variables that hold username and password:
```js
constructor(){
        super();
        this.state={
            username: '',
            password: ''
        }
    }
```
If you look at this code all two stsates make sense: 
```js
<label>Username
    <input type='text' name='username' placeholder='username' onChange={(e)=>{this.setState({username: e.target.val}/>
</label>
<label>Password
    <input type='password' name='password' placeholder='password' onChange={(e)=>{this.setState({passwe.target.value})}}/>
</label>
```
Above, we implemented that, once user changes the anything inside input that change will be recorder either in `this.state.username` or `this.state.password`. 
* Next we have made `Mutation`. In this case `Mutation` component gets three props, which are `mutation`,`onCompleted`, and `onError`. `mutation` just like query in prop in `Query` is gets mutation object namely `SIGN_UP`. `onCompleted` and `onError` are unique for Mutation. Unlike `Query`, these props get callback function that will be fired according to the state of the request to graphql endpoint. `onCompleted` is fired when the request is successfully completed. `onError` is fired when the request is responsed with some kind of error.
```js
<Mutation mutation = {SIGN_UP} onCompleted={()=>{alert('Signup complete!')}} onError={()=>{alert('Error happened!')}}>
    {}
</Mutation>
```
* Last, inside Mutation, we have defined some javascript code. Here, like `Query` in the provious section, we are defining an arrow function. The difference is that, this function gets the name of the function that will be fired when the user interacts with the page. In the code we difined the interaction to be `onClick`. It is important to mention that, `Mutation` does not send request to server when the page loads, but when some user interaction like `onClick`, `onMouseEnter`, and etc. happens. But, in `Query` request to server is sent when the page loads, precisely when the Component is rendered. It is logical, because in the case of `Query` user interaction is only to load the page and the rest is done by the page itself and `Query` is not meant to change anything in the server, but just fetching data.
```js
{(signUpWithPassword)=>{
    return(
      <div>
          <form>
              <label>Username
                  <input type='text' name='username' placeholder='username' onChange={(e)=>{this.setState({username: e.target.value})}}/>
              </label>
              <label>Password
                  <input type='password' name='password' placeholder='password' onChange={(e)=>{this.setState({password: e.target.value})}}/>
              </label>
              <button type='submit' onClick={(e)=>{
                  e.preventDefault()
                  signUpWithPassword({variables:{username: this.state.username, password: this.state.password}})
              }}>SignUp</button>
          </form>
      </div>
    )   
}}
```


### Article Challange
Look at `onCompleted` props of Mutation we implemented so far. Now, try to pass a callback function that saves coming data(`access token`) into `localStorage`. Please make sure that item name to be `accessToken`.
Javascript code to save any data into `localStorage` is: 
```js
localStorage.setItem(itemName, variable);
```
`itemName` can be customized. If you set new item into `localStorage`, you can access it in `Chrome Developer Tools` => `Application` menu. 
>The read-only `localStorage` property allows you to access a Storage object for the Document's origin; the stored data is saved across browser sessions. localStorage is similar to sessionStorage, except that while data stored in localStorage has no expiration time, data stored in sessionStorage gets cleared when the page session ends — that is, when the page is closed. To learn more about it, visit [here](https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage)

In the next Chapter, We will explore how to implement authorization in Apollo Client.