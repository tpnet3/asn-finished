# 쿼리로 데이터 페치하기
ApolloProvider 가 일단 연결되면, 쿼리 컴포넌트를 가지는 데이터를 요청할 수 있습니다. 쿼리는 
UI와 함께 GraphQL 데이터를 쉐아하는 prop 패턴을 렌더링하는데 이용하는 react-apollo 로부터 불러온 React 컴포넌트 입니다.

`gql` 함수에 있는 `GraphQL` 쿼리를 `Query` 컴포넌트에 쿼리 prop로 넘겨주세요. 그러면 무엇을 렌더링 할지 결정할 Query 컴포넌트의 자식 prop으로 함수를 제공할 것 입니다. 이는 `loading`, `error`,  `data` 속성을 포함하는 객체와 함께 쿼리가 호출 할 것 입니다. 아폴로 클라이언트는 에러를 트랙킹하고 state를 로딩할 것 이며 이는 `loading` 와 `error` 속성에서 반영이 될 것 입니다. 한번 쿼리의 결과가 오면, ‘data’ 속성에 첨부가 될 것 입니다.
‘

실제 사례를 둘러봅시다
Grapqhl로부터의 데이터를 페치하기 위해서는 App.js 파일을 수정할 것 입니다 <br>
다음과 같이 해보세요
* src 폴더 내에 App.js 로 가서 필요한 모듈을 임포트 하세요

    ```js
    import { Query } from "react-apollo";
    import gql from "graphql-tag";
    ```
* `<Query>`는 graphql 종단점으로부터 데이터를 페치하는데 이용하는 컴포넌트 입니다<br>
  * `gql`은 graphql 쿼리 오프젝트로 문자열을 변환할 수 있도록 도와주는 모듈입니다.

* 다음으로는, 쿼리 문자열을 만들어봅시다
    ```js
    const GET_ITEM = gql`
    query getItem($_id: String!){
        getItem(_id: $_id){
            name
            author
            price
            pics
            short_desc
            long_desc
            date_added
        }
    }
    `
    ```

* 위 코드에서 보듯, 쿼리 백틱(`)과 함께 쿼리 문자열을 gql에 포함해줍니다. 이는 쿼리 객체를 생성합니다.
* 그럼 다음과 같이 쿼리를 완성할 수 있습니다
    ```js 
    class App extends Component {
    render() {
        return (
            <Query query={GET_ITEM} variables={{_id:'5c544c28149c161e5dfff12c'}}>
            {({loading, data, error})=>{
                if(loading)return<p>Loading ...</p>
                if(error)return<p>{error}</p>
                //if both error and loading are false this means data has some returned value
                var jsDate = new Date(data.getItem.date_added);
                return(
                <div className="App">
                <header className="App-header">
                {data.getItem.pics.map(pic=>{
                    return <img key={pic} src={pic} className="App-logo" alt="logo" />
                })}
                <p className='title'>
                    {data.getItem.name}
                </p>
                <p>by <b>{data.getItem.author}</b></p>
                <p>Price <b>{data.getItem.price}$</b></p>
                <p>Description:</p>
                <p>Date Added: <b>{jsDate.toUTCString()}</b></p>
                </header>
            </div>
                )
            }}
            </Query>
            );
        }
        }

    export default App;
    ```
* App.css 파일로 App.js에 사용되어진 스타일을 추가 하는 것이 또 하나의 방법입니다. 
    ```css
    .image {
    height: 40vmin;
    pointer-events: none;
    margin: 10px;
    }
    .title{
    max-width: 500px;
    text-align: center;
    }
    ```

### 도전하기
* 위 `GET_ITEM` 쿼리로부터 보듯, book의  the short_desc(short description) and long_desc(long description) 을 페치하였습니다. 이제 App.js  내에 그것들을 접근해보고 `<blockquote>`  태그를  스크린에 띄어보세요

다음 장으로는 뮤테이션을 활용하여 graphql 종단점에 어떻게 코드를 작성하는지 배워보겠습니다. 


