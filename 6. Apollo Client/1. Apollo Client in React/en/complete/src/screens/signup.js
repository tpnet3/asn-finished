import React, { Component } from 'react'
import {Mutation} from 'react-apollo'
import gql from 'graphql-tag'

const SIGN_UP = gql`
mutation signUpWithPassword($username:String!, $password: String!){
  signUpWithPassword(username: $username, password: $password)
}
`

export default class Signup extends Component {
    constructor(){
        super();
        this.state={
            username: '',
            password: ''
        }
    }
  render() {
    return (
    <Mutation mutation = {SIGN_UP} onCompleted={()=>{alert('Signup complete!')}} onError={()=>{alert('Error happened!')}}>
      {(signUpWithPassword)=>{
          return(
            <div>
                <form>
                    <label>Username
                        <input type='text' name='username' placeholder='username' onChange={(e)=>{this.setState({username: e.target.value})}}/>
                    </label>
                    <label>Password
                        <input type='password' name='password' placeholder='password' onChange={(e)=>{this.setState({password: e.target.value})}}/>
                    </label>
                    <button type='submit' onClick={(e)=>{
                        e.preventDefault()
                        signUpWithPassword({variables:{username: this.state.username, password: this.state.password}})
                    }}>SignUp</button>
                </form>
            </div>
          )   
      }}
    </Mutation>
    )
  }
}
